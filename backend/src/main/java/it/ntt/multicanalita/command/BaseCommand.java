package it.ntt.multicanalita.command;

import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.utils.LoggerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseCommand<R> {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

    protected R doExecute() throws Exception {
        return null;
    }

    public R execute() throws Exception {
        if(canExecute())
            return doExecute();
        else
            throw new MulticanalitaForbiddenException(LoggerUtils.formatArchRow("Cannot execute command"));
    }
}
