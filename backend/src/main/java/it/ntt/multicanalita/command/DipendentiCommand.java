package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import it.ntt.multicanalita.converter.InitialConverter;
import it.ntt.multicanalita.dto.InitialDTO;
import it.ntt.multicanalita.dto.InputDTO;
import it.ntt.multicanalita.entity.DipendentiEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.InitService;


@Component
@Scope("prototype")
public class DipendentiCommand extends BaseCommand<InitialDTO> {
 
    @Autowired
    private InitService service;
 
    @Autowired
    private InitialConverter converter;
    
	private final InputDTO input;
	
	public DipendentiCommand(InputDTO params) {
		this.input = params;
	}
	
	@Override
    public InitialDTO doExecute() throws Exception {	
        long startTime = System.currentTimeMillis();
       
        String response = this.service.callService(this.input);
        List<DipendentiEntity> dipendenti = this.service.getDipendenti(); 
		logger.info("dipendenti={}|esito=OK", "RequestId",dipendenti);
	       
		logger.info("requestId={}|operazione=initial|esito=OK|durataOperazione={}{}", "RequestId",(System.currentTimeMillis() - startTime)," ms");
       
		return this.converter.convertInitialString(response,dipendenti); 
    }

    @Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}