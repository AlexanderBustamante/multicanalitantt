package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.EvolutivaService;


@Component
@Scope("prototype")
public class EvolutivaDeleteByIdCommand extends BaseCommand<Boolean>{
	@Autowired 
	private EvolutivaService service;
	private final Long idEvolutiva ;
	
	public EvolutivaDeleteByIdCommand(Long idevolutiva) {
		this.idEvolutiva = idevolutiva;
	}
	
	@Override
	public Boolean doExecute() throws Exception{
		return this.service.deleteEvolutivaById(idEvolutiva);
	}
	@Override
	public boolean canExecute() throws MulticanalitaForbiddenException{
		return true;
	}
		
	
}
