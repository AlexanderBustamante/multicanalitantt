package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.EvolutivaConverter;
import it.ntt.multicanalita.dto.EvolutivaDTO;
import it.ntt.multicanalita.entity.EvolutivaEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.EvolutivaService;

@Component
@Scope("prototype")
public class EvolutivaGetByIdCommand extends BaseCommand<EvolutivaDTO>{
	
	 	@Autowired
	    private EvolutivaService service; 
	 
	    @Autowired
	    private EvolutivaConverter converter;
	    
		private final Long idEvo;
		
		public EvolutivaGetByIdCommand(Long idEvo) {
			this.idEvo = idEvo;
		}
		
		@Override
		public EvolutivaDTO doExecute() throws Exception {
			
			long startTime = System.currentTimeMillis(); 
			
			EvolutivaEntity evolutiva = this.service.getEvolutivaById(this.idEvo);
			logger.info("requestId={}|operazione=initial|esito=OK|durataOperazione={}{}", "RequestId",(System.currentTimeMillis() - startTime)," ms");
		
			return this.converter.converterEvolutivaEntityToEvolutivaDTO(evolutiva);
		}
		
		@Override 
	    protected boolean canExecute() throws MulticanalitaForbiddenException {
	        return true;
	    }
}
