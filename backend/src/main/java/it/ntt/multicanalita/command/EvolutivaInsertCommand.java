package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartException;

import it.ntt.multicanalita.converter.EvolutivaConverter;
import it.ntt.multicanalita.dto.EvolutivaDTO;
import it.ntt.multicanalita.entity.EvolutivaEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.EvolutivaService;



@Component
@Scope("prototype")
public class EvolutivaInsertCommand extends BaseCommand<EvolutivaDTO> {
 
    @Autowired
    private EvolutivaService service; 
 
    @Autowired
    private EvolutivaConverter converter;
    
	private final EvolutivaDTO evoInput;
	
	public EvolutivaInsertCommand(EvolutivaDTO params) {
		this.evoInput = params;
	}
	
	@Override
    public EvolutivaDTO doExecute() throws Exception {	
        long startTime = System.currentTimeMillis();
        if(evoInput == null) {
        	throw new MulticanalitaForbiddenException("evolutiva NULL");
        }
        
        EvolutivaEntity evolutiva = this.converter.converterEvolutivaDTOToEvolutivaEntity(this.evoInput);
        EvolutivaEntity evoEntity = this.service.insert(evolutiva);
        
		return this.converter.converterEvolutivaEntityToEvolutivaDTO(evoEntity); 
    }

    @Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

}