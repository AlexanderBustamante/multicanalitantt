package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.EvolutivaConverter;
import it.ntt.multicanalita.dto.EvolutivaDTO;
import it.ntt.multicanalita.entity.EvolutivaEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.EvolutivaService;

@Component
@Scope("prototype")
public class EvolutivaUpdateCommand extends BaseCommand<EvolutivaDTO> {
	
	@Autowired
    private EvolutivaService service; 
 
    @Autowired
    private EvolutivaConverter converter;
    
	private final EvolutivaDTO input;
	
	public EvolutivaUpdateCommand(EvolutivaDTO params) {
		this.input = params;
	}
	@Override
    public EvolutivaDTO doExecute() throws Exception {	

        EvolutivaEntity evolutivaTemp = this.converter.converterEvolutivaDTOToEvolutivaEntity(this.input); 
        EvolutivaEntity evolutiva = this.service.update(evolutivaTemp);
        
		return this.converter.converterEvolutivaEntityToEvolutivaDTO(evolutiva); 
    }
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
	

}
