package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import it.ntt.multicanalita.converter.EvolutivaConverter;
import it.ntt.multicanalita.dto.EvolutivaDTO;

import it.ntt.multicanalita.entity.EvolutivaEntity;

import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.EvolutivaService;

@Component
@Scope("prototype")
public class EvolutiveCommand extends BaseCommand<List<EvolutivaDTO>>{
	@Autowired
	private EvolutivaService service;
	@Autowired
	private EvolutivaConverter converter;
	
	@Override
	public List<EvolutivaDTO> doExecute() throws Exception{	
		List<EvolutivaEntity> evolutive = this.service.getEvolutive();
		return this.converter.converterEvolutiveEntityToEvolutiveDTO(evolutive); 
	}
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

}
