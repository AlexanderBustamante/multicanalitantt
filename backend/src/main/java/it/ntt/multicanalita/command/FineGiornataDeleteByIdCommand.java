package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.FineGiornataService;
import it.ntt.multicanalita.service.UserService;

@Component
@Scope("prototype")
public class FineGiornataDeleteByIdCommand extends BaseCommand<Boolean> {
	@Autowired 
	private FineGiornataService service;
	
	private final Long idGiornata;
	
	public FineGiornataDeleteByIdCommand(Long idInput) {
		this.idGiornata = idInput;
	}
	
	@Override
	public Boolean doExecute() throws Exception {
		long startTime = System.currentTimeMillis(); 

		Boolean fineGiornataDelete = this.service.deleteFineGiornataById(this.idGiornata);

		return fineGiornataDelete;
	}
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}