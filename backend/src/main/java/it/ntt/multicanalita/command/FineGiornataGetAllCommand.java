package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.FineGiornataConverter;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.FineGiornataService;


@Component
@Scope("prototype")
public class FineGiornataGetAllCommand extends BaseCommand<List<FineGiornataDTO>>{
	@Autowired
	private FineGiornataService service;
	@Autowired
	private FineGiornataConverter converter;
	
	
	@Override
	public List<FineGiornataDTO> doExecute() throws Exception{	
		List<FineGiornataEntity> fineGiornata = service.getAllFineGiornata();
		//logger.info("fineGiornata dopo getall:{}  ",fineGiornata.toString());
		return this.converter.converterFineGiornataEntityTOFineGiornataDTO(fineGiornata); 
	}
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

}
