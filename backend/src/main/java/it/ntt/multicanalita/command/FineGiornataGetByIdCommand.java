package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.FineGiornataConverter;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.FineGiornataService;

@Component
@Scope("prototype")
public class FineGiornataGetByIdCommand extends BaseCommand<FineGiornataDTO>{
	@Autowired
	private FineGiornataService service;
	@Autowired
	private FineGiornataConverter converter;
	
	private final Long idGiornata;
	
	public FineGiornataGetByIdCommand (Long tempId) {
		
		this.idGiornata = tempId;
		logger.info("temp id passata in finegiornata getbyid = {}",this.idGiornata);
	}
//	//CHIEDERE AD ALE
//	public FineGiornataGetByIdCommand () {
//		
//		this.idGiornata = 1L;
//		logger.info("temp id passata in finegiornata getbyid = {}",this.idGiornata);
//	}
	
	
	
	
	@Override
	public FineGiornataDTO doExecute() throws Exception{	
		FineGiornataEntity fineGiornata = service.getById(this.idGiornata);
	//	logger.info("fineGiornata dopo getByid:{}  ",fineGiornata.toString());
		return this.converter.converterFineGiornataEntityTOFineGiornataDTO(fineGiornata); 
	}
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}
