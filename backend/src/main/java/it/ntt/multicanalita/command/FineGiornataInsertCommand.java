package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import it.ntt.multicanalita.converter.FineGiornataConverter;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.entity.EvolutivaFineGiornataEntity;
import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.EvolutivaFineGiornataService;
import it.ntt.multicanalita.service.FineGiornataService;

@Component
@Scope("prototype")
public class FineGiornataInsertCommand extends BaseCommand<FineGiornataDTO> {
 
    @Autowired
    private FineGiornataService service; 
    @Autowired
    private EvolutivaFineGiornataService serviceEvoFinGio;
    @Autowired
    private FineGiornataConverter converter;
    
	private final FineGiornataDTO fGInput;
	
	public FineGiornataInsertCommand(FineGiornataDTO params) {
		this.fGInput = params;
	}
	
	@Override
    public FineGiornataDTO doExecute() throws Exception {	
		
        if(fGInput == null) {
        	throw new MulticanalitaForbiddenException("fine giornata NULL");
        }
        logger.info("Fine giornata in input:{}", this.fGInput);
        FineGiornataEntity fineGiornata = this.converter.converterFineGiornataDTOToFineGiornataEntity(this.fGInput);
       
        
//        for(int i=0; fineGiornata.getEvolutivaFineGiornata().length())
        	
        	
        	
        logger.info("Fine giornata da inserire:{}", fineGiornata);
        FineGiornataEntity FGEntity = this.service.insert(fineGiornata);
        List<EvolutivaFineGiornataEntity> list = FGEntity.getEvolutivaFineGiornata();
        for(EvolutivaFineGiornataEntity i : list) {
        	i.setFineGiornata(FGEntity);
        	EvolutivaFineGiornataEntity EFGEntity = this.serviceEvoFinGio.insert(i);
        }
//        EvolutivaFineGiornataEntity a = list.get(0);
//        a.setFineGiornata(FGEntity);
//        EvolutivaFineGiornataEntity EFGEntity = this.serviceEvoFinGio.insert(a);
        
        
 
        
		return this.converter.converterFineGiornataEntityTOFineGiornataDTO(FGEntity); 
    }

    @Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

}