package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.FineGiornataConverter;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.FineGiornataService;


@Component
@Scope("prototype")
public class FineGiornataUpdateCommand extends BaseCommand<FineGiornataDTO> {
	
	@Autowired
    private FineGiornataService service; 
 
    @Autowired
    private FineGiornataConverter converter;
    
	private final FineGiornataDTO input;
	
	public FineGiornataUpdateCommand(FineGiornataDTO params) {
		this.input = params;
	}
	@Override
    public FineGiornataDTO doExecute() throws Exception {	

        FineGiornataEntity fineGiornataTemp = this.converter.converterFineGiornataDTOToFineGiornataEntity(this.input); 
        FineGiornataEntity fineGiornata = this.service.update(fineGiornataTemp);
        
		return this.converter.converterFineGiornataEntityTOFineGiornataDTO(fineGiornata); 
    }
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
	

}
