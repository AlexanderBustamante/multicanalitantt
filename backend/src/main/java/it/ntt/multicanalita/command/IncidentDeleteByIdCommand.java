package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.IncidentService;


@Component
@Scope("prototype")
public class IncidentDeleteByIdCommand extends BaseCommand<Boolean>{
	@Autowired 
	private IncidentService service;
	private final Long idIncident ;
	
	public IncidentDeleteByIdCommand(Long idIncident) {
		this.idIncident = idIncident;
	}
	
	@Override
	public Boolean doExecute() throws Exception{
		return this.service.deleteIncidentById(idIncident);
	}
	@Override
	public boolean canExecute() throws MulticanalitaForbiddenException{
		return true;
	}
		
	
}
