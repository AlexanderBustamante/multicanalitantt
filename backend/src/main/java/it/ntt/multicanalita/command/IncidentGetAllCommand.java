package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.FineGiornataConverter;
import it.ntt.multicanalita.converter.IncidentConverter;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.dto.IncidentDTO;
import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.entity.IncidentEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.FineGiornataService;
import it.ntt.multicanalita.service.IncidentService;


@Component
@Scope("prototype")
public class IncidentGetAllCommand extends BaseCommand<List<IncidentDTO>>{
	@Autowired
	private IncidentService service;
	@Autowired
	private IncidentConverter converter;
	
	
	@Override
	public List<IncidentDTO> doExecute() throws Exception{	
		List<IncidentEntity> incident = service.getAllIncident();
//		logger.info("incident dopo getall:{}  ",incident.toString());
		return this.converter.converterIncidentEntityToIncidentDTO(incident); 
	}
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

}
