package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import it.ntt.multicanalita.converter.IncidentConverter;
import it.ntt.multicanalita.dto.IncidentDTO;
import it.ntt.multicanalita.entity.IncidentEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.IncidentService;

@Component
@Scope("prototype")
public class IncidentGetByIdCommand extends BaseCommand<IncidentDTO>{
	@Autowired
	private IncidentService service;
	@Autowired
	private IncidentConverter converter;
	
	private final Long idinc;
	
	public IncidentGetByIdCommand (Long tempId) {
		this.idinc = tempId;
	}
	
	
	@Override
	public IncidentDTO doExecute() throws Exception{	
		IncidentEntity incident = service.getById(this.idinc);
	//	logger.info("fineGiornata dopo getByid:{}  ",fineGiornata.toString());
		return this.converter.converterIncidentEntityToIncidentDTO(incident); 
	}
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}
