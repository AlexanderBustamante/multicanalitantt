package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.IncidentConverter;
import it.ntt.multicanalita.dto.IncidentDTO;
import it.ntt.multicanalita.entity.IncidentEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.IncidentService;

@Component
@Scope("prototype")
public class IncidentInsertCommand extends BaseCommand<List<IncidentDTO>> {
 
    @Autowired
    private IncidentService service; 
 
    @Autowired
    private IncidentConverter converter;
    
	private final List<IncidentDTO> incInput;
	
	public IncidentInsertCommand(List<IncidentDTO> params) {
		this.incInput = params;
	}
	
	@Override
    public List<IncidentDTO> doExecute() throws Exception {	
		
        if(incInput == null) {
        	throw new MulticanalitaForbiddenException("incident NULL");
        }
        
        List<IncidentEntity> incident = this.converter.converterIncidentDTOToIncidentEntity(this.incInput);
        List<IncidentEntity> incEntity = this.service.insert(incident);
        
		return this.converter.converterIncidentEntityToIncidentDTO(incEntity); 
    }

    @Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

}