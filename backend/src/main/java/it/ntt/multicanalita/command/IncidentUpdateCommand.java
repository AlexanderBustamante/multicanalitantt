package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.IncidentConverter;
import it.ntt.multicanalita.dto.IncidentDTO;
import it.ntt.multicanalita.entity.IncidentEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.IncidentService;

@Component
@Scope("prototype")
public class IncidentUpdateCommand extends BaseCommand<IncidentDTO> {
	
	@Autowired
    private IncidentService service; 
 
    @Autowired
    private IncidentConverter converter;
    
	private final IncidentDTO input;
	
	public IncidentUpdateCommand(IncidentDTO params) {
		this.input = params;
	}
	@Override
    public IncidentDTO doExecute() throws Exception {	

        IncidentEntity incidentTemp = this.converter.converterIncidentDTOToIncidentEntity(this.input); 
        IncidentEntity incident = this.service.update(incidentTemp);
        
		return this.converter.converterIncidentEntityToIncidentDTO(incident); 
    }
	
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
	

}
