package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import it.ntt.multicanalita.converter.InitialConverter;
import it.ntt.multicanalita.dto.InitialDTO;
import it.ntt.multicanalita.dto.InputDTO;
import it.ntt.multicanalita.entity.DipendentiEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.InitService;

@Component
@Scope("prototype")
public class InitialCommand extends BaseCommand<InitialDTO> {
 
    @Autowired
    private InitService service;
 
    @Autowired
    private InitialConverter converter;
    
	private final InputDTO input;
	
	public InitialCommand(InputDTO params) {
		this.input = params;
	}
	
	@Override
    public InitialDTO doExecute() throws Exception {	
        long startTime = System.currentTimeMillis();
       
        String response = this.service.callService(this.input);
        List<DipendentiEntity> dipendenti = this.service.getDipendenti(); 
        
        DipendentiEntity entity = this.service.getDipendentiByNome(this.input.getInit()); 
        
		logger.info("dipendenti={}|esito=OK", "RequestId",dipendenti);
	       
		logger.info("requestId={}|operazione=initial|esito=OK|durataOperazione={}{}", "RequestId",(System.currentTimeMillis() - startTime)," ms");
       
		return this.converter.convertInitialString(entity.getCognome(),dipendenti); 
    }

    @Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}