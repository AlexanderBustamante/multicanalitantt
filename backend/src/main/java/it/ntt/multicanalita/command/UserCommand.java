package it.ntt.multicanalita.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.UserConverter;
import it.ntt.multicanalita.dto.pure.UserPureDTO;
import it.ntt.multicanalita.entity.UserEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.UserService;

@Component
@Scope("prototype")
public class UserCommand extends BaseCommand<List<UserPureDTO>> {
	@Autowired 
	private UserService service;
	
	@Autowired 
	private UserConverter converter;
	
	@Override
	public List<UserPureDTO> doExecute() throws Exception{	
		List<UserEntity> users = this.service.getUsers();
		return this.converter.convertUsersEntityToUsersDTO(users); 
	}
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }

}
