package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.UserService;

@Component
@Scope("prototype")
public class UserDeleteByIdCommand extends BaseCommand<Boolean> {
	@Autowired 
	private UserService service;
	
	private final Long idUser;
	
	public UserDeleteByIdCommand(Long idUser) {
		this.idUser = idUser;
	}
	
	@Override
	public Boolean doExecute() throws Exception {
		long startTime = System.currentTimeMillis(); 

		Boolean userDelete = this.service.deleteUserById(this.idUser);

		return userDelete;
	}
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}