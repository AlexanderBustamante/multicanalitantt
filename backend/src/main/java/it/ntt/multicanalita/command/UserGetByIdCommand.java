package it.ntt.multicanalita.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converter.UserConverter;
import it.ntt.multicanalita.dto.pure.UserPureDTO;
import it.ntt.multicanalita.entity.UserEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.UserService;

@Component
@Scope("prototype")
public class UserGetByIdCommand extends BaseCommand<UserPureDTO> {
	@Autowired 
	private UserService service;
	
	@Autowired 
	private UserConverter converter;
	
	private final Long idUser;
	
	public UserGetByIdCommand(Long idUser) {
		this.idUser = idUser;
	}
	
	@Override
	public  UserPureDTO doExecute() throws Exception {
		
		long startTime = System.currentTimeMillis(); 
		
		UserEntity user = this.service.getUserById(this.idUser);
		logger.info("requestId={}|operazione=initial|esito=OK|durataOperazione={}{}", "RequestId",(System.currentTimeMillis() - startTime)," ms");
		
		return this.converter.convertUserEntityToUserDTO(user);
	}
	@Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}
