package it.ntt.multicanalita.command;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import it.ntt.multicanalita.converter.UserConverter;
import it.ntt.multicanalita.dto.pure.UserPureDTO;
import it.ntt.multicanalita.entity.UserEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaForbiddenException;
import it.ntt.multicanalita.service.UserService;

@Component
@Scope("prototype")
public class UserInsertCommand extends BaseCommand<UserPureDTO> {
 
    @Autowired
    private UserService service; 
 
    @Autowired
    private UserConverter converter;
    
	private final UserPureDTO input;
	
	public UserInsertCommand(UserPureDTO params) {
		this.input = params;
	}
	
	@Override
    public UserPureDTO doExecute() throws Exception {	
        long startTime = System.currentTimeMillis();
        UserEntity user = this.converter.convertUserDTOToUserEntity(this.input); 
        UserEntity userEntity = this.service.insert(user);
        
		return this.converter.convertUserEntityToUserDTO(userEntity); 
    }

    @Override 
    protected boolean canExecute() throws MulticanalitaForbiddenException {
        return true;
    }
}