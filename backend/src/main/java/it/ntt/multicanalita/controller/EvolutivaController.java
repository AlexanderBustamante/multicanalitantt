package it.ntt.multicanalita.controller;

import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.ntt.multicanalita.command.EvolutivaDeleteByIdCommand;
import it.ntt.multicanalita.command.EvolutivaGetByIdCommand;
import it.ntt.multicanalita.command.EvolutivaInsertCommand;
import it.ntt.multicanalita.command.EvolutivaUpdateCommand;
import it.ntt.multicanalita.command.EvolutiveCommand;
import it.ntt.multicanalita.command.UserGetByIdCommand;
import it.ntt.multicanalita.dto.EvolutivaDTO;
import it.ntt.multicanalita.dto.pure.UserPureDTO;

@RestController
@RequestMapping(value = "evolutiva")
public class EvolutivaController extends BaseController {

	@Autowired
	private BeanFactory beanFactory;
	
	@GetMapping
	public ResponseEntity<List<EvolutivaDTO>> all() throws Exception{
		logger.info("Entro nel controller Evolutive");

		EvolutiveCommand command = beanFactory.getBean(EvolutiveCommand.class);
		List<EvolutivaDTO> result = command.execute();
		
		return ResponseEntity.ok(result);
	}
	@GetMapping("/{id}")
	public ResponseEntity<EvolutivaDTO> getById(@PathVariable Long id) throws Exception{
		logger.info("requestId={}|operazione=validazioneRequest|esito=OK", "123456");
		long startTime = System.currentTimeMillis();

		
		EvolutivaGetByIdCommand command = beanFactory.getBean(EvolutivaGetByIdCommand.class,id);
		EvolutivaDTO result = command.execute();
		logger.info("requestId={}|richiesta=fine|durataOperazione={}{}", "654321",(System.currentTimeMillis() - startTime)," ms");
		return ResponseEntity.ok(result);
		
	}

	@PostMapping("/insert") 
	public ResponseEntity<EvolutivaDTO> insertEvolutiva(@RequestBody EvolutivaDTO evolutiva) throws Exception{
		logger.info("EvolutivaDTO in entrata ={}|operazione=chiamato il controller insert", evolutiva.toString());
		
		EvolutivaInsertCommand command = beanFactory.getBean(EvolutivaInsertCommand.class, evolutiva);
		EvolutivaDTO result = command.execute();

		return ResponseEntity.ok(result);
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteUserById(@PathVariable Long id) throws Exception{
		logger.debug("inzio controller delete evolutiva con id: " + id);
		EvolutivaDeleteByIdCommand command = beanFactory.getBean(EvolutivaDeleteByIdCommand.class,id);
		Boolean result = command.execute();
		return ResponseEntity.ok(result);
	}
	
	@PutMapping("/update")
	public  ResponseEntity<EvolutivaDTO> updateEvolutiva(@RequestBody EvolutivaDTO varEvolutiva) throws Exception{
		logger.info("requestId={}|operazione=validazioneRequest|esito=OK", "123456");
		EvolutivaUpdateCommand command = beanFactory.getBean(EvolutivaUpdateCommand.class, varEvolutiva);
		EvolutivaDTO result = command.execute();
		
		return ResponseEntity.ok(result);
		
	}
}