package it.ntt.multicanalita.controller;

import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.ntt.multicanalita.command.FineGiornataDeleteByIdCommand;
import it.ntt.multicanalita.command.FineGiornataGetAllCommand;
import it.ntt.multicanalita.command.FineGiornataGetByIdCommand;
import it.ntt.multicanalita.command.FineGiornataInsertCommand;
import it.ntt.multicanalita.command.FineGiornataUpdateCommand;
import it.ntt.multicanalita.dto.FineGiornataDTO;


@RestController
@RequestMapping(value = "finegiornata")
public class FineGiornataController extends BaseController {
	
	@Autowired
	private BeanFactory beanFactory;
	
	@GetMapping("/all")
	public ResponseEntity<List<FineGiornataDTO>> all() throws Exception{
		FineGiornataGetAllCommand command = beanFactory.getBean(FineGiornataGetAllCommand.class);
		List<FineGiornataDTO> result = command.execute();
		return ResponseEntity.ok(result);
		
	}
	@GetMapping("/{id}")
	public ResponseEntity<FineGiornataDTO> giornataByid(@PathVariable Long id) throws Exception{
		FineGiornataGetByIdCommand command = beanFactory.getBean(FineGiornataGetByIdCommand.class,id);
		FineGiornataDTO result = command.execute();
		return ResponseEntity.ok(result);
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Boolean> deleteFineGiornataById(@PathVariable Long id) throws Exception{	
		
		FineGiornataDeleteByIdCommand command =  beanFactory.getBean(FineGiornataDeleteByIdCommand.class,id);
		Boolean result = command.execute();
		return ResponseEntity.ok(result);
		
	}
	
	@PutMapping("/update")
	public  ResponseEntity<FineGiornataDTO> updateFineGiornata(@RequestBody FineGiornataDTO varFineGiornata) throws Exception{
		FineGiornataUpdateCommand command = beanFactory.getBean(FineGiornataUpdateCommand.class, varFineGiornata);
		FineGiornataDTO result= command.execute();
		
		return ResponseEntity.ok(result);	
	}
	
	@PostMapping("/insert") 
	public ResponseEntity<FineGiornataDTO> insertFineGiornata(@RequestBody FineGiornataDTO fineGiornata) throws Exception{

		FineGiornataInsertCommand command = beanFactory.getBean(FineGiornataInsertCommand.class, fineGiornata);
		FineGiornataDTO result = command.execute();

		return ResponseEntity.ok(result);
	}
}
