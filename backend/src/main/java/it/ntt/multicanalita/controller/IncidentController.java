package it.ntt.multicanalita.controller;

import java.util.List;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.ntt.multicanalita.command.EvolutiveCommand;
import it.ntt.multicanalita.command.FineGiornataGetByIdCommand;
import it.ntt.multicanalita.command.IncidentDeleteByIdCommand;
import it.ntt.multicanalita.command.IncidentGetAllCommand;
import it.ntt.multicanalita.command.IncidentGetByIdCommand;
import it.ntt.multicanalita.command.IncidentInsertCommand;
import it.ntt.multicanalita.command.IncidentUpdateCommand;
import it.ntt.multicanalita.dto.EvolutivaDTO;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.dto.IncidentDTO;

@RestController
@RequestMapping(value = "incident")
public class IncidentController {
	
	@Autowired
	private BeanFactory beanFactory;
	
	@GetMapping
	public ResponseEntity<List<IncidentDTO>> all() throws Exception{

		IncidentGetAllCommand command = beanFactory.getBean(IncidentGetAllCommand.class);
		List<IncidentDTO> result = command.execute();
		
		return ResponseEntity.ok(result);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<IncidentDTO> incidentByid(@PathVariable Long id) throws Exception{
		IncidentGetByIdCommand command = beanFactory.getBean(IncidentGetByIdCommand.class,id);
		IncidentDTO result = command.execute();
		return ResponseEntity.ok(result);
		
	}
	
	@PostMapping("/insert") 
	public ResponseEntity<List<IncidentDTO>> insertIncident(@RequestBody List<IncidentDTO> incidenti) throws Exception{

		IncidentInsertCommand command = beanFactory.getBean(IncidentInsertCommand.class, incidenti);
		List<IncidentDTO> result = command.execute();

		return ResponseEntity.ok(result);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteIncidentById(@PathVariable Long id) throws Exception{
		IncidentDeleteByIdCommand command = beanFactory.getBean(IncidentDeleteByIdCommand.class, id);
		Boolean result = command.execute();
		return ResponseEntity.ok(result);
	}
	
	@PutMapping("/update")
	public  ResponseEntity<IncidentDTO> updateIncident(@RequestBody IncidentDTO varIncident) throws Exception{
		IncidentUpdateCommand command = beanFactory.getBean(IncidentUpdateCommand.class, varIncident);
		IncidentDTO result= command.execute();
		
		return ResponseEntity.ok(result);	
	}
}
