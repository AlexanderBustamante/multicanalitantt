package it.ntt.multicanalita.controller;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.ntt.multicanalita.command.InitialCommand;
import it.ntt.multicanalita.dto.InitialDTO;
import it.ntt.multicanalita.dto.InputDTO;

@RestController
@RequestMapping(value = "api")
public class InitialController extends BaseController {

	@Autowired
	private BeanFactory beanFactory;
	
	@GetMapping("/init")
	public ResponseEntity<InitialDTO> init() throws Exception {
		logger.info("requestId={}|operazione=validazioneRequest|esito=OK", "123456");
		
		long startTime = System.currentTimeMillis();
		
		InputDTO request = new InputDTO(); 
		request.setInit("Marisa");
		
		InitialCommand command = beanFactory.getBean(InitialCommand.class, request);
		InitialDTO result = command.execute();
		
		logger.info("requestId={}|richiesta=fine|durataOperazione={}{}", "654321",(System.currentTimeMillis() - startTime)," ms");

		return ResponseEntity.ok(result);
	}
}