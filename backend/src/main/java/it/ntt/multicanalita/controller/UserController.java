package it.ntt.multicanalita.controller;

import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.ntt.multicanalita.command.UserCommand;
import it.ntt.multicanalita.command.UserDeleteByIdCommand;
import it.ntt.multicanalita.command.UserGetByIdCommand;
import it.ntt.multicanalita.command.UserInsertCommand;
import it.ntt.multicanalita.command.UserUpdateCommand;
import it.ntt.multicanalita.dto.pure.UserPureDTO;

@RestController
@RequestMapping(value = "user")
public class UserController extends BaseController{
	
	@Autowired
	private BeanFactory beanFactory;
	
	@GetMapping
	public ResponseEntity<List<UserPureDTO>> all() throws Exception{
		logger.info("requestId={}|operazione=validazioneRequest|esito=OK", "123456");

		UserCommand command = beanFactory.getBean(UserCommand.class);
		List<UserPureDTO> result = command.execute();
		
		return ResponseEntity.ok(result);
	}
	@GetMapping("/{id}")
	public ResponseEntity<UserPureDTO> getById(@PathVariable Long id) throws Exception{
		logger.info("requestId={}|operazione=validazioneRequest|esito=OK", "123456");
		long startTime = System.currentTimeMillis();

		
		UserGetByIdCommand command = beanFactory.getBean(UserGetByIdCommand.class,id);
		UserPureDTO result = command.execute();
		logger.info("requestId={}|richiesta=fine|durataOperazione={}{}", "654321",(System.currentTimeMillis() - startTime)," ms");
		return ResponseEntity.ok(result);
		
	}
	
	@DeleteMapping("/{id}")
	
	public ResponseEntity<Boolean> deleteUserById(@PathVariable Long id) throws Exception{	
		
		UserDeleteByIdCommand command = beanFactory.getBean(UserDeleteByIdCommand.class,id);
		Boolean result = command.execute();
		return ResponseEntity.ok(result);
		
	}
	
	@PostMapping("/insert") 
	public ResponseEntity<UserPureDTO> insertUser(@RequestBody UserPureDTO user) throws Exception{
		logger.info("requestId={}|operazione=validazioneRequest|esito=OK", "123456");
		
		UserInsertCommand command = beanFactory.getBean(UserInsertCommand.class, user);
		UserPureDTO result = command.execute();

		return ResponseEntity.ok(result);
	}
	
	@PutMapping("/update")
	public  ResponseEntity<UserPureDTO> updateUser(@RequestBody UserPureDTO varUser) throws Exception{
		logger.info("requestId={}|operazione=validazioneRequest|esito=OK", "123456");
		UserUpdateCommand command = beanFactory.getBean(UserUpdateCommand.class, varUser);
		UserPureDTO result = command.execute();
		
		return ResponseEntity.ok(result);
		
	}
}
