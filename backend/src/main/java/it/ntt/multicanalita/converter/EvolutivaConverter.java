package it.ntt.multicanalita.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.dto.EvolutivaDTO;
import it.ntt.multicanalita.dto.pure.UserPureDTO;
import it.ntt.multicanalita.entity.EvolutivaEntity;
import it.ntt.multicanalita.entity.UserEntity;


@Component
public class EvolutivaConverter {
	@Autowired 
	private UserConverter converter;
	
	public List<EvolutivaDTO> converterEvolutiveEntityToEvolutiveDTO(List<EvolutivaEntity> evolutive) {
		return evolutive.stream().map(evolutiva -> 
		converterEvolutivaEntityToEvolutivaDTO(evolutiva)	
		).collect(Collectors.toList());
	}

	
	public EvolutivaEntity converterEvolutivaDTOToEvolutivaEntity(EvolutivaDTO input){
		EvolutivaEntity evolutivaEntity = new EvolutivaEntity();
		evolutivaEntity.setId(input.getId());
		evolutivaEntity.setAcronimo(input.getAcronimo());
		evolutivaEntity.setRichiedenteISP(input.getRichiedenteISP());
		evolutivaEntity.setRefNTTD(input.getRefNTTD());
		evolutivaEntity.setDescrizione(input.getDescrizione());
		evolutivaEntity.setEvolutivaReleasePROD(input.isEvolutivaReleasePROD());
		evolutivaEntity.setTipoRichiesta(input.getTipoRichiesta());
		evolutivaEntity.setLoiNumOrdineNTTD(input.getLoiNumOrdineNTTD());
		evolutivaEntity.setStatoTesorettoEvo(input.getStatoTesorettoEvo());
		evolutivaEntity.setRdaISP(input.getRdaISP());
		evolutivaEntity.setTecSviluppo(input.getTecSviluppo());
		evolutivaEntity.setDtApproStima(input.getDtApproStima());
		evolutivaEntity.setDtRichiesta(input.getDtRichiesta());
		evolutivaEntity.setDtInvioStima(input.getDtInvioStima());
		evolutivaEntity.setDtDocTecnica(input.getDtDocTecnica());
		evolutivaEntity.setDtInoltroOfferta(input.getDtInoltroOfferta());
		evolutivaEntity.setDtAvvioAttivita(input.getDtAvvioAttivita());
		evolutivaEntity.setDtStartSvil(input.getDtStartSvil());
		evolutivaEntity.setDtSystem(input.getDtSystem());
		evolutivaEntity.setDtProd(input.getDtProd());
		evolutivaEntity.setStato(input.getStato());
		evolutivaEntity.setStimaGGUISP(input.getStimaGGUISP());
		evolutivaEntity.setGgUtilizzateISP(input.getGgUtilizzateISP());
		evolutivaEntity.setResiduoISP(input.getResiduoISP());
		evolutivaEntity.setStimaKISP(input.getStimaKISP());
		evolutivaEntity.setStimaKISPNoIVA(input.getStimaKISPNoIVA());
		evolutivaEntity.setGgUtiliNNTD (input.getGgUtiliNNTD());
		evolutivaEntity.setOreNTTDUtilizzate (input.getOreNTTDUtilizzate());
		evolutivaEntity.setOreUtiliNTTD (input.getOreUtiliNTTD());
		evolutivaEntity.setGgUtilizzatiNTTD (input.getGgUtilizzatiNTTD());
		evolutivaEntity.setResiduoGGISP (input.getResiduoGGISP());
		evolutivaEntity.setResiduoGGNTTD (input.getResiduoGGNTTD());
		evolutivaEntity.setTariffaFerNTTD (input.getTariffaFerNTTD());
		evolutivaEntity.setRefrealizzaznttd (this.converter.convertUserDTOToUserEntity(input.getRefRealizzazNTTD()) );
		evolutivaEntity.setBasketOreResiduo (input.getBasketOreResiduo());
		evolutivaEntity.setBasketGGResiduo (input.getBasketGGResiduo());
		evolutivaEntity.setBasketISPGGResiduo (input.getBasketISPGGResiduo());
		evolutivaEntity.setSottrarreATesoretto(input.getSottrarreATesoretto());
		evolutivaEntity.setDispTesoResiduoDaAut(input.getDispTesoResiduoDaAut());
		evolutivaEntity.setDispTesoResiduoAut(input.getDispTesoResiduoAut());
		evolutivaEntity.setGgTesoTot(input.getGgTesoTot());
		return evolutivaEntity;
		
	}
	public EvolutivaDTO converterEvolutivaEntityToEvolutivaDTO(EvolutivaEntity input) {
		EvolutivaDTO evolutivaDTO = new EvolutivaDTO();
		evolutivaDTO.setId(input.getId());
		evolutivaDTO.setAcronimo(input.getAcronimo());
		evolutivaDTO.setRichiedenteISP(input.getRichiedenteISP());
		evolutivaDTO.setRefNTTD(input.getRefNTTD());
		evolutivaDTO.setDescrizione(input.getDescrizione());
		evolutivaDTO.setEvolutivaReleasePROD(input.isEvolutivaReleasePROD());
		evolutivaDTO.setTipoRichiesta(input.getTipoRichiesta());
		evolutivaDTO.setLoiNumOrdineNTTD(input.getLoiNumOrdineNTTD());
		evolutivaDTO.setStatoTesorettoEvo(input.getStatoTesorettoEvo());
		evolutivaDTO.setRdaISP(input.getRdaISP());
		evolutivaDTO.setTecSviluppo(input.getTecSviluppo());
		evolutivaDTO.setDtApproStima(input.getDtApproStima());
		evolutivaDTO.setDtRichiesta(input.getDtRichiesta());
		evolutivaDTO.setDtInvioStima(input.getDtInvioStima());
		evolutivaDTO.setDtDocTecnica(input.getDtDocTecnica());
		evolutivaDTO.setDtInoltroOfferta(input.getDtInoltroOfferta());
		evolutivaDTO.setDtAvvioAttivita(input.getDtAvvioAttivita());
		evolutivaDTO.setDtStartSvil(input.getDtStartSvil());
		evolutivaDTO.setDtSystem(input.getDtSystem());
		evolutivaDTO.setDtProd(input.getDtProd());
		evolutivaDTO.setStato(input.getStato());
		evolutivaDTO.setStimaGGUISP(input.getStimaGGUISP());
		evolutivaDTO.setGgUtilizzateISP(input.getGgUtilizzateISP());
		evolutivaDTO.setResiduoISP(input.getResiduoISP());
		evolutivaDTO.setStimaKISP(input.getStimaKISP());
		evolutivaDTO.setStimaKISPNoIVA(input.getStimaKISPNoIVA());
		evolutivaDTO.setGgUtiliNNTD (input.getGgUtiliNNTD());
		evolutivaDTO.setOreNTTDUtilizzate (input.getOreNTTDUtilizzate());
		evolutivaDTO.setOreUtiliNTTD (input.getOreUtiliNTTD());
		evolutivaDTO.setGgUtilizzatiNTTD (input.getGgUtilizzatiNTTD());
		evolutivaDTO.setResiduoGGISP (input.getResiduoGGISP());
		evolutivaDTO.setResiduoGGNTTD (input.getResiduoGGNTTD());
		evolutivaDTO.setTariffaFerNTTD (input.getTariffaFerNTTD());
		evolutivaDTO.setRefRealizzazNTTD (this.converter.convertUserEntityToUserDTO(input.getRefrealizzaznttd()));
		evolutivaDTO.setBasketOreResiduo (input.getBasketOreResiduo());
		evolutivaDTO.setBasketGGResiduo (input.getBasketGGResiduo());
		evolutivaDTO.setBasketISPGGResiduo (input.getBasketISPGGResiduo());
		evolutivaDTO.setSottrarreATesoretto(input.getSottrarreATesoretto());
		evolutivaDTO.setDispTesoResiduoDaAut(input.getDispTesoResiduoDaAut());
		evolutivaDTO.setDispTesoResiduoAut(input.getDispTesoResiduoAut());
		evolutivaDTO.setGgTesoTot(input.getGgTesoTot());
		return evolutivaDTO;
		
		
	}

}
