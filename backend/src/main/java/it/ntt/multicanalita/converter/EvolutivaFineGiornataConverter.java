package it.ntt.multicanalita.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.command.BaseCommand;
import it.ntt.multicanalita.converterPure.EvolutivaConverterPure;
import it.ntt.multicanalita.converterPure.FineGiornataConverterPure;
import it.ntt.multicanalita.dto.EvolutivaFineGiornataDTO;
import it.ntt.multicanalita.entity.EvolutivaFineGiornataEntity;

@Component
public class EvolutivaFineGiornataConverter extends BaseCommand<EvolutivaFineGiornataConverter> {
	@Autowired
	private FineGiornataConverterPure fineGiornataConverter;
	@Autowired
	private EvolutivaConverterPure evoConverter;
	
	public EvolutivaFineGiornataDTO converterEvolutivaEvolutivaFineGiornataEntityTOEvolutivaFineGiornataDTO(EvolutivaFineGiornataEntity input) {
		EvolutivaFineGiornataDTO output = new EvolutivaFineGiornataDTO();
		
		output.setId(input.getId());
		output.setOreEvo(input.getOreEvo());
		output.setEvolutiva(evoConverter.converterEvolutivaEntityToEvolutivaPureDTO(input.getEvolutive()));
		output.setFineGiornata(fineGiornataConverter.converterFineGiornataEntityTOFineGiornataPureDTO(input.getFineGiornata()));
		return output;
	}
	public List<EvolutivaFineGiornataDTO> converterEvolutivaEvolutivaFineGiornataEntityTOEvolutivaFineGiornataDTO(List<EvolutivaFineGiornataEntity> input){
	return input.stream().map(evofinegiornata -> 
	converterEvolutivaEvolutivaFineGiornataEntityTOEvolutivaFineGiornataDTO(evofinegiornata)	
	).collect(Collectors.toList());
	}
	
	
	public EvolutivaFineGiornataEntity converterEvolutivaFineGiornataDTOToEvolutivaFineGiornataEntity(EvolutivaFineGiornataDTO input) {
		logger.info("converterEvolutivaFineGiornataDTOToEvolutivaFineGiornataEntity, input: ", input.toString());
		EvolutivaFineGiornataEntity output = new EvolutivaFineGiornataEntity();
		
		if(input.getId()!= null){
			output.setId(input.getId());
		}
		output.setOreEvo(input.getOreEvo());
		output.setEvolutive(evoConverter.converterEvolutivaPureDTOToEvolutivaEntity(input.getEvolutiva()));
		output.setFineGiornata(fineGiornataConverter.converterFineGiornataPureDTOFinegiornataEntity(input.getFineGiornata()));
		return output;
	}
	public List<EvolutivaFineGiornataEntity> converterEvolutivaFineGiornataDTOToEvolutivaFineGiornataEntity(List<EvolutivaFineGiornataDTO> input){
	return input.stream().map(evofinegiornata -> 
	converterEvolutivaFineGiornataDTOToEvolutivaFineGiornataEntity(evofinegiornata)	
	).collect(Collectors.toList());
	}
	

}
