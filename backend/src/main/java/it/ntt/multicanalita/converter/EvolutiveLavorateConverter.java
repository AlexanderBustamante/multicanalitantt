package it.ntt.multicanalita.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import it.ntt.multicanalita.dto.EvolutivaFineGiornataDTO;
import it.ntt.multicanalita.dto.pure.EvolutivaLavorataPureDTO;
@Component
public class EvolutiveLavorateConverter {
	public EvolutivaLavorataPureDTO converterEvolutivaFineGiornataDTOToEvolutivaLavorataPureDTO(EvolutivaFineGiornataDTO input) {
		EvolutivaLavorataPureDTO output = new EvolutivaLavorataPureDTO();
		output.setEvo(input.getEvolutiva());
		output.setOreEvo(input.getOreEvo());
		return output;	
	}
	public List<EvolutivaLavorataPureDTO> converterEvolutivaFineGiornataDTOToEvolutivaLavorataPureDTO(List<EvolutivaFineGiornataDTO> input){
		return input.stream().map(out -> 
		converterEvolutivaFineGiornataDTOToEvolutivaLavorataPureDTO(out)	
		).collect(Collectors.toList());	
	}
	
	public EvolutivaFineGiornataDTO converterEvolutivaLavorataPureDTOToEvolutivaFineGiornataDTO(EvolutivaLavorataPureDTO input) {
		EvolutivaFineGiornataDTO output = new EvolutivaFineGiornataDTO();
		output.setEvolutiva(input.getEvo());
		output.setOreEvo(input.getOreEvo());
		return output;	
	}
	public List<EvolutivaFineGiornataDTO> converterEvolutivaLavorataPureDTOToEvolutivaFineGiornataDTO(List<EvolutivaLavorataPureDTO> input){
		return input.stream().map(out -> 
		converterEvolutivaLavorataPureDTOToEvolutivaFineGiornataDTO(out)	
		).collect(Collectors.toList());	
	}
}
