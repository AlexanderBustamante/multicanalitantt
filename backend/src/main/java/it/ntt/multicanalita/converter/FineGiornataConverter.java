package it.ntt.multicanalita.converter;


import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import it.ntt.multicanalita.command.BaseCommand;
import it.ntt.multicanalita.converterPure.IncidentConverterPure;
import it.ntt.multicanalita.dto.EvolutivaFineGiornataDTO;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.entity.FineGiornataEntity;

@Component
public class FineGiornataConverter extends BaseCommand<FineGiornataEntity> {
	@Autowired 
	private UserConverter userConverter;
	@Autowired
	private IncidentConverterPure incidentConverter;
	@Autowired
	private EvolutiveLavorateConverter evoLavConverter;
	@Autowired
	private EvolutivaFineGiornataConverter evoFinConverter;
	
	public FineGiornataDTO converterFineGiornataEntityTOFineGiornataDTO(FineGiornataEntity input) {
		FineGiornataDTO fineGiornataDTO = new FineGiornataDTO();
		fineGiornataDTO.setId(input.getId());
		fineGiornataDTO.setOreAm(input.getOreAM());
		fineGiornataDTO.setOreEvo(input.getOreEvo());
		fineGiornataDTO.setOreExtra(input.getOreExtra());
		fineGiornataDTO.setOreHandover(input.getOreHandover());
		fineGiornataDTO.setFeedback(input.getFeedback());
		fineGiornataDTO.setGiornata(input.getGiornata());
		fineGiornataDTO.setUser(userConverter.convertUserEntityToUserDTO(input.getUtente()));
		if(input.getIncidentLavorati()==null) {
			fineGiornataDTO.setIncidentLavorati(null);
		} 
		else {
			fineGiornataDTO.setIncidentLavorati(incidentConverter.converterIncidentEntityToIncidentPureDTO(input.getIncidentLavorati()));
		}
		if(input.getEvolutivaFineGiornata()==null) {
			fineGiornataDTO.setEvolutiveLavorate(null);
		}
		else {
			fineGiornataDTO.setEvolutiveLavorate(evoLavConverter.converterEvolutivaFineGiornataDTOToEvolutivaLavorataPureDTO( evoFinConverter.converterEvolutivaEvolutivaFineGiornataEntityTOEvolutivaFineGiornataDTO(input.getEvolutivaFineGiornata())));
		}
		return fineGiornataDTO;
	}
	public List<FineGiornataDTO> converterFineGiornataEntityTOFineGiornataDTO(List<FineGiornataEntity> input){
		return input.stream().map(finegiornata -> 
		converterFineGiornataEntityTOFineGiornataDTO(finegiornata)	
		).collect(Collectors.toList());
	}
		
	public FineGiornataEntity converterFineGiornataDTOToFineGiornataEntity (FineGiornataDTO input) {
		FineGiornataEntity fineGiornataEntity = new FineGiornataEntity();
		if(input.getId()!=null) {
			fineGiornataEntity.setId(input.getId());
		}
		
		fineGiornataEntity.setOreAM(input.getOreAm());
		fineGiornataEntity.setOreEvo(input.getOreEvo());
		fineGiornataEntity.setOreExtra(input.getOreExtra());
		fineGiornataEntity.setOreHandover(input.getOreHandover());
		fineGiornataEntity.setFeedback(input.getFeedback());
		fineGiornataEntity.setGiornata(input.getGiornata());
		fineGiornataEntity.setUtente(userConverter.convertUserDTOToUserEntity(input.getUser()));
		fineGiornataEntity.setIncidentLavorati(incidentConverter.converterIncidentPureDTOToIncidentEntity(input.getIncidentLavorati()));
		
		List<EvolutivaFineGiornataDTO> out =  evoLavConverter.converterEvolutivaLavorataPureDTOToEvolutivaFineGiornataDTO(input.getEvolutiveLavorate());
		logger.info("EvolutivaFineGiornataDTO {}",out);
		fineGiornataEntity.setEvolutivaFineGiornata(evoFinConverter.converterEvolutivaFineGiornataDTOToEvolutivaFineGiornataEntity(out)); 
		return fineGiornataEntity;
	}
	public List<FineGiornataEntity> converterFineGiornataDTOToFineGiornataEntity(List<FineGiornataDTO> input){
		return input.stream().map(finegiornata -> 
		converterFineGiornataDTOToFineGiornataEntity(finegiornata)	
		).collect(Collectors.toList());
	}
}
