package it.ntt.multicanalita.converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.ntt.multicanalita.converterPure.FineGiornataConverterPure;
import it.ntt.multicanalita.dto.IncidentDTO;
import it.ntt.multicanalita.entity.IncidentEntity;

@Component
public class IncidentConverter {
	@Autowired
	private FineGiornataConverterPure fineGiornataConverter;
	
	public IncidentDTO converterIncidentEntityToIncidentDTO(IncidentEntity input) {
		IncidentDTO incidentDTO = new IncidentDTO();
		
		incidentDTO.setId(input.getId());
		incidentDTO.setClusterAcronimo(input.getClusterAcronimo());
		incidentDTO.setAcronimo(input.getAcronimo());
		incidentDTO.setnIncident(input.getnIncident());
		incidentDTO.setRequestType(input.getRequestType());
		incidentDTO.setEsitoInt(input.getEsitoInt());
		incidentDTO.setTipoAss(input.getTipoAss());		
		incidentDTO.setSystem(input.getSystem());
		
		incidentDTO.setFineGiornata(fineGiornataConverter.converterFineGiornataEntityTOFineGiornataPureDTO(input.getFineGiornata()));
		return incidentDTO;
		
	}
	public List<IncidentDTO> converterIncidentEntityToIncidentDTO(List<IncidentEntity> input){
		return input.stream().map(Incident -> 
		converterIncidentEntityToIncidentDTO(Incident)	
		).collect(Collectors.toList());
		
		
		
	}
	
	public IncidentDTO converterIncidentEntityToIncidentDTOwithOutBinding(IncidentEntity input) {
		IncidentDTO incidentDTO = new IncidentDTO();
		
		incidentDTO.setId(input.getId());
		incidentDTO.setClusterAcronimo(input.getClusterAcronimo());
		incidentDTO.setAcronimo(input.getAcronimo());
		incidentDTO.setnIncident(input.getnIncident());
		incidentDTO.setRequestType(input.getRequestType());
		incidentDTO.setEsitoInt(input.getEsitoInt());
		incidentDTO.setTipoAss(input.getTipoAss());
		incidentDTO.setSystem(input.getSystem());
		return incidentDTO;
		
	}
	public List<IncidentDTO> converterIncidentEntityToIncidentDTOwithOutBinding(List<IncidentEntity> input){
		return input.stream().map(Incident -> 
		converterIncidentEntityToIncidentDTOwithOutBinding(Incident)	
		).collect(Collectors.toList());
		
		
		
	}
	
	public IncidentEntity converterIncidentDTOToIncidentEntity(IncidentDTO input){
		IncidentEntity incidentEntity = new IncidentEntity();
		incidentEntity.setId(input.getId());
		incidentEntity.setClusterAcronimo(input.getClusterAcronimo());
		incidentEntity.setAcronimo(input.getAcronimo());
		incidentEntity.setnIncident(input.getnIncident());
		incidentEntity.setRequestType(input.getRequestType());
		incidentEntity.setEsitoInt(input.getEsitoInt());
		incidentEntity.setTipoAss(input.getTipoAss());
		incidentEntity.setSystem(input.getSystem());
		incidentEntity.setFineGiornata(fineGiornataConverter.converterFineGiornataPureDTOFinegiornataEntity(input.getFineGiornata()));
		
		return incidentEntity;
	}
	
	public List<IncidentEntity> converterIncidentDTOToIncidentEntity(List<IncidentDTO> input){
		return input.stream().map(Incident -> 
		converterIncidentDTOToIncidentEntity(Incident)	
		).collect(Collectors.toList());
	}
}
