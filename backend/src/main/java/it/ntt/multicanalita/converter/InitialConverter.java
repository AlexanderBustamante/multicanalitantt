package it.ntt.multicanalita.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import it.ntt.multicanalita.dto.InitialDTO;
import it.ntt.multicanalita.entity.DipendentiEntity;

@Component
public class InitialConverter {

	public InitialDTO convertInitialString(String response, List<DipendentiEntity> dipendenti) {		
		InitialDTO initialDTO = new InitialDTO(); 
		initialDTO.setDipendentiEntity(dipendenti);
		initialDTO.setInit(response);
		
		return initialDTO;
	}
}