package it.ntt.multicanalita.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import it.ntt.multicanalita.dto.pure.UserPureDTO;
import it.ntt.multicanalita.entity.UserEntity;

@Component
public class UserConverter {
	
	public List<UserPureDTO> convertUsersEntityToUsersDTO(List<UserEntity> users) {
		return users.stream().map(user -> 
			convertUserEntityToUserDTO(user)	
		).collect(Collectors.toList());
	}

	public UserEntity convertUserDTOToUserEntity(UserPureDTO input) {
		UserEntity userEntity = new UserEntity();
		userEntity.setId(input.getId());
		userEntity.setCognome(input.getCognome());
		userEntity.setEmail(input.getEmail());
		userEntity.setNome(input.getNome());
		userEntity.setIdCluApp(input.getIdCluApp());
		userEntity.setPosizione(input.getPosizione());
		userEntity.setRuolo(input.getRuolo());
		
		return userEntity;
	}

	public UserPureDTO convertUserEntityToUserDTO(UserEntity userEntity) {
		UserPureDTO user = new UserPureDTO(); 
		user.setId(userEntity.getId());
		user.setCognome(userEntity.getCognome());
		user.setEmail(userEntity.getEmail());
		user.setNome(userEntity.getNome());
		user.setIdCluApp(userEntity.getIdCluApp());
		user.setPosizione(userEntity.getPosizione());
		user.setRuolo(userEntity.getRuolo());
		
		return user;
	}
}
