package it.ntt.multicanalita.converterPure;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.qos.logback.classic.Logger;
import it.ntt.multicanalita.converter.UserConverter;
import it.ntt.multicanalita.dto.FineGiornataDTO;
import it.ntt.multicanalita.dto.pure.FineGiornataPureDTO;
import it.ntt.multicanalita.entity.FineGiornataEntity;
@Component
public class FineGiornataConverterPure {
	@Autowired 
	private UserConverter userConverter;
	
	public FineGiornataPureDTO converterFineGiornataEntityTOFineGiornataPureDTO(FineGiornataEntity input) {
		FineGiornataPureDTO fineGiornataDTO = new FineGiornataPureDTO();
		if(input == null) {
			return null;
		}
		fineGiornataDTO.setId(input.getId());
		fineGiornataDTO.setOreAm(input.getOreAM());
		fineGiornataDTO.setOreEvo(input.getOreEvo());
		fineGiornataDTO.setOreExtra(input.getOreExtra());
		fineGiornataDTO.setOreHandover(input.getOreHandover());
		fineGiornataDTO.setFeedback(input.getFeedback());
		fineGiornataDTO.setGiornata(input.getGiornata());
		fineGiornataDTO.setUser(userConverter.convertUserEntityToUserDTO(input.getUtente()));

		
		
		return fineGiornataDTO;
	}
	public List<FineGiornataPureDTO> converterFineGiornataEntityTOFineGiornataPureDTO(List<FineGiornataEntity> input){
		if (!(input == null)){
			return input.stream().map(finegiornata -> 
			converterFineGiornataEntityTOFineGiornataPureDTO(finegiornata)	
			).collect(Collectors.toList());
		}
		return null;
	}

	
	public FineGiornataEntity converterFineGiornataPureDTOFinegiornataEntity(FineGiornataPureDTO input) {
		FineGiornataEntity fineGiornataEntity = new FineGiornataEntity();
		if(input==null) {
			return  null;
		}
		if(input.getId()!=null) {
			fineGiornataEntity.setId(input.getId());
		}
		fineGiornataEntity.setOreAM(input.getOreAm());
		fineGiornataEntity.setOreEvo(input.getOreEvo());
		fineGiornataEntity.setOreExtra(input.getOreExtra());
		fineGiornataEntity.setOreHandover(input.getOreHandover());
		fineGiornataEntity.setFeedback(input.getFeedback());
		fineGiornataEntity.setGiornata(input.getGiornata());
		fineGiornataEntity.setUtente(userConverter.convertUserDTOToUserEntity(input.getUser()));
		return fineGiornataEntity;
	}
	public List<FineGiornataEntity> converterFineGiornataPureDTOFinegiornataEntity(List<FineGiornataPureDTO> input){
		if (!(input == null)){
			return input.stream().map(finegiornata -> 
			converterFineGiornataPureDTOFinegiornataEntity(finegiornata)	
					).collect(Collectors.toList());
		}
		return null;
		
	}

}
