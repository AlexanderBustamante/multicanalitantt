package it.ntt.multicanalita.converterPure;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;


import it.ntt.multicanalita.dto.pure.IncidentPureDTO;
import it.ntt.multicanalita.entity.IncidentEntity;

@Component
public class IncidentConverterPure {
	public IncidentPureDTO converterIncidentEntityToIncidentPureDTO(IncidentEntity input) {
		
		IncidentPureDTO incidentPureDTO = new IncidentPureDTO();
		incidentPureDTO.setId(input.getId());
		incidentPureDTO.setClusterAcronimo(input.getClusterAcronimo());
		incidentPureDTO.setAcronimo(input.getAcronimo());
		incidentPureDTO.setnIncident(input.getnIncident());
		incidentPureDTO.setRequestType(input.getRequestType());
		incidentPureDTO.setEsitoInt(input.getEsitoInt());
		incidentPureDTO.setTipoAss(input.getTipoAss());
		incidentPureDTO.setSystem(input.getSystem());
		return incidentPureDTO;

	}

	public List<IncidentPureDTO> converterIncidentEntityToIncidentPureDTO(List<IncidentEntity> input) {
		return input.stream().map(Incident -> converterIncidentEntityToIncidentPureDTO(Incident))
				.collect(Collectors.toList());

	}
public IncidentEntity converterIncidentPureDTOToIncidentEntity( IncidentPureDTO input) {
		
		IncidentEntity out = new IncidentEntity();
		if(input.getId()!=null) {
			out.setId(input.getId());	
		}
		out.setClusterAcronimo(input.getClusterAcronimo());
		out.setAcronimo(input.getAcronimo());
		out.setnIncident(input.getnIncident());
		out.setRequestType(input.getRequestType());
		out.setEsitoInt(input.getEsitoInt());
		out.setTipoAss(input.getTipoAss());
		out.setSystem(input.getSystem());
		return out;

	}

	public List<IncidentEntity> converterIncidentPureDTOToIncidentEntity(List<IncidentPureDTO> input) {
		return input.stream().map(Incident -> converterIncidentPureDTOToIncidentEntity(Incident))
				.collect(Collectors.toList());

	}

}
