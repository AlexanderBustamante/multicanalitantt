package it.ntt.multicanalita.dto;

import it.ntt.multicanalita.dto.pure.EvolutivaPureDTO;
import it.ntt.multicanalita.dto.pure.FineGiornataPureDTO;

public class EvolutivaFineGiornataDTO {
	
	private Long id;
	private Float oreEvo;
	private EvolutivaPureDTO evolutiva;
	private FineGiornataPureDTO fineGiornata;
	
	public EvolutivaPureDTO getEvolutiva() {
		return evolutiva;
	}
	public void setEvolutiva(EvolutivaPureDTO evolutiva) {
		this.evolutiva = evolutiva;
	}
	public FineGiornataPureDTO getFineGiornata() {
		return fineGiornata;
	}
	public void setFineGiornata(FineGiornataPureDTO fineGiornata) {
		this.fineGiornata = fineGiornata;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Float getOreEvo() {
		return oreEvo;
	}
	@Override
	public String toString() {
		return "EvolutivaFineGiornataDTO [id=" + id + ", oreEvo=" + oreEvo + ", evolutiva=" + evolutiva
				+ ", fineGiornata=" + fineGiornata + "]";
	}
	public void setOreEvo(Float oreEvo) {
		this.oreEvo = oreEvo;
	}

}
