package it.ntt.multicanalita.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;


import it.ntt.multicanalita.dto.pure.EvolutivaLavorataPureDTO;
import it.ntt.multicanalita.dto.pure.IncidentPureDTO;
import it.ntt.multicanalita.dto.pure.UserPureDTO;

public class FineGiornataDTO {
	
	private Long id;
	private Float oreAm;
	private Float oreEvo;
	private Float oreExtra;
	private Float oreCorsi;
	private Float oreHandover;
	private Integer feedback;
	private Date giornata;
	
	private UserPureDTO user;
	private List<IncidentPureDTO> incidentLavorati;
	//private HashMap<EvolutivaDTO, Float> evolutiveLavorate;
	private List<EvolutivaLavorataPureDTO> evolutiveLavorate;
	
	
	public UserPureDTO getUser() {
		return user;
	}
	public void setUser(UserPureDTO user) {
		this.user = user;
	}
	
	public List<IncidentPureDTO> getIncidentLavorati() {
		return incidentLavorati;
	}
	public void setIncidentLavorati(List<IncidentPureDTO> incidentLavorati) {
		this.incidentLavorati = incidentLavorati;
	}
	
	
	
	public List<EvolutivaLavorataPureDTO> getEvolutiveLavorate() {
		return evolutiveLavorate;
	}
	public void setEvolutiveLavorate(List<EvolutivaLavorataPureDTO> evolutiveLavorate) {
		this.evolutiveLavorate = evolutiveLavorate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Float getOreAm() {
		return oreAm;
	}
	public void setOreAm(Float oreAm) {
		this.oreAm = oreAm;
	}
	public Float getOreEvo() {
		return oreEvo;
	}
	public void setOreEvo(Float oreEvo) {
		this.oreEvo = oreEvo;
	}
	public Float getOreExtra() {
		return oreExtra;
	}
	public void setOreExtra(Float oreExtra) {
		this.oreExtra = oreExtra;
	}
	public Float getOreCorsi() {
		return oreCorsi;
	}
	public void setOreCorsi(Float oreCorsi) {
		this.oreCorsi = oreCorsi;
	}
	public Float getOreHandover() {
		return oreHandover;
	}
	public void setOreHandover(Float oreHandover) {
		this.oreHandover = oreHandover;
	}
	public Integer getFeedback() {
		return feedback;
	}
	public void setFeedback(Integer feedback) {
		this.feedback = feedback;
	}
	public Date getGiornata() {
		return giornata;
	}
	public void setGiornata(Date giornata) {
		this.giornata = giornata;
	}
	@Override
	public String toString() {
		return "FineGiornataDTO [id=" + id + ", oreAm=" + oreAm + ", oreEvo=" + oreEvo + ", oreExtra=" + oreExtra
				+ ", oreCorsi=" + oreCorsi + ", oreHandover=" + oreHandover + ", feedback=" + feedback + ", giornata="
				+ giornata + ", user=" + user + ", incidentLavorati=" + incidentLavorati + ", evolutiveLavorate="
				+ evolutiveLavorate + "]";
	}
	
	
	
	

}
