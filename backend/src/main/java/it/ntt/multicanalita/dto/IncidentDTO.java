package it.ntt.multicanalita.dto;

import java.util.List;
import java.util.Set;

import it.ntt.multicanalita.dto.pure.FineGiornataPureDTO;

public class IncidentDTO {
	
	private Long id;
	private String  clusterAcronimo;
	private String  acronimo;
	private String	nIncident;
	private String requestType;
	private String esitoInt;
	private String tipoAss;
	private String gdlRiAss;
	private String note;
	private Boolean system;
	private String fte;
	private List<FineGiornataPureDTO> fineGiornata;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClusterAcronimo() {
		return clusterAcronimo;
	}

	public void setClusterAcronimo(String clusterAcronimo) {
		this.clusterAcronimo = clusterAcronimo;
	}

	public String getAcronimo() {
		return acronimo;
	}

	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}

	public String getnIncident() {
		return nIncident;
	}

	public void setnIncident(String nIncident) {
		this.nIncident = nIncident;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getEsitoInt() {
		return esitoInt;
	}

	public void setEsitoInt(String esitoInt) {
		this.esitoInt = esitoInt;
	}

	public String getTipoAss() {
		return tipoAss;
	}

	public void setTipoAss(String tipoAss) {
		this.tipoAss = tipoAss;
	}

	public String getGdlRiAss() {
		return gdlRiAss;
	}

	public void setGdlRiAss(String gdlRiAss) {
		this.gdlRiAss = gdlRiAss;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Boolean getSystem() {
		return system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public String getFte() {
		return fte;
	}

	public void setFte(String fte) {
		this.fte = fte;
	}

	public List<FineGiornataPureDTO> getFineGiornata() {
		return fineGiornata;
	}

	public void setFineGiornata(List<FineGiornataPureDTO> fineGiornata) {
		this.fineGiornata = fineGiornata;
	}

	
	
	
}
