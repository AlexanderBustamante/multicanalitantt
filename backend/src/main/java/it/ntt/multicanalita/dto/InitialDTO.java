package it.ntt.multicanalita.dto;

import java.util.List;

import it.ntt.multicanalita.entity.DipendentiEntity;

public class InitialDTO  {
	private List<DipendentiEntity> dipendentiEntity;
	private String init;

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	public List<DipendentiEntity> getDipendentiEntity() {
		return dipendentiEntity;
	}

	public void setDipendentiEntity(List<DipendentiEntity> dipendentiEntity) {
		this.dipendentiEntity = dipendentiEntity;
	}
}
