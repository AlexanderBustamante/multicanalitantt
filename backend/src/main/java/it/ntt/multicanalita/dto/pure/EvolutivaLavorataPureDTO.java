package it.ntt.multicanalita.dto.pure;

public class EvolutivaLavorataPureDTO {
	private Float oreEvo;
	private EvolutivaPureDTO Evo;
	public Float getOreEvo() {
		return oreEvo;
	}
	public void setOreEvo(Float oreEvo) {
		this.oreEvo = oreEvo;
	}
	public EvolutivaPureDTO getEvo() {
		return Evo;
	}
	public void setEvo(EvolutivaPureDTO evo) {
		Evo = evo;
	}
	@Override
	public String toString() {
		return "EvolutivaLavorataPureDTO [oreEvo=" + oreEvo + ", Evo=" + Evo + "]";
	}
	

}
