package it.ntt.multicanalita.dto.pure;

import java.util.Date;

public class EvolutivaPureDTO {

	private Long id;
    private String acronimo;
    private String richiedenteISP;
    private String refNTTD;
    private String descrizione;
    private boolean evolutivaReleasePROD;
    private String tipoRichiesta;
    private String loiNumOrdineNTTD;
    private String statoTesorettoEvo;
    private String rdaISP;
    private String tecSviluppo;
	private Date dtApproStima;
	private Date dtRichiesta;
	private Date dtInvioStima;
	private Date dtDocTecnica;
	private Date dtInoltroOfferta;
	private Date dtAvvioAttivita;
	private Date dtStartSvil;
	private Date dtSystem;
	private Date dtProd;
	private String stato;
	private Integer stimaGGUISP;
	private Float ggUtilizzateISP;
	private Float residuoISP;
	private Integer stimaKISP;
	private Integer stimaKISPNoIVA;
	private Integer ggUtiliNNTD;
	private Integer oreNTTDUtilizzate;
	private Integer oreUtiliNTTD;
	private Integer ggUtilizzatiNTTD;
	private Integer residuoGGISP;
	private Integer residuoGGNTTD;
	private Float tariffaFerNTTD;
	private UserPureDTO refRealizzazNTTD;
	private Integer basketOreResiduo;
	private Integer basketGGResiduo;
	private Integer basketISPGGResiduo;
	private Integer sottrarreATesoretto;
	private Integer dispTesoResiduoDaAut;
	private Integer dispTesoResiduoAut;
	private Integer ggTesoTot;
	
	//Getters and Setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAcronimo() {
		return acronimo;
	}
	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}
	public String getRichiedenteISP() {
		return richiedenteISP;
	}
	public void setRichiedenteISP(String richiedenteISP) {
		this.richiedenteISP = richiedenteISP;
	}
	public String getRefNTTD() {
		return refNTTD;
	}
	public void setRefNTTD(String refNTTD) {
		this.refNTTD = refNTTD;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public boolean isEvolutivaReleasePROD() {
		return evolutivaReleasePROD;
	}
	public void setEvolutivaReleasePROD(boolean evolutivaReleasePROD) {
		this.evolutivaReleasePROD = evolutivaReleasePROD;
	}
	public String getTipoRichiesta() {
		return tipoRichiesta;
	}
	public void setTipoRichiesta(String tipoRichiesta) {
		this.tipoRichiesta = tipoRichiesta;
	}
	public String getLoiNumOrdineNTTD() {
		return loiNumOrdineNTTD;
	}
	public void setLoiNumOrdineNTTD(String loiNumOrdineNTTD) {
		this.loiNumOrdineNTTD = loiNumOrdineNTTD;
	}
	public String getStatoTesorettoEvo() {
		return statoTesorettoEvo;
	}
	public void setStatoTesorettoEvo(String statoTesorettoEvo) {
		this.statoTesorettoEvo = statoTesorettoEvo;
	}
	public String getRdaISP() {
		return rdaISP;
	}
	public void setRdaISP(String rdaISP) {
		this.rdaISP = rdaISP;
	}
	public String getTecSviluppo() {
		return tecSviluppo;
	}
	public void setTecSviluppo(String tecSviluppo) {
		this.tecSviluppo = tecSviluppo;
	}
	public Date getDtApproStima() {
		return dtApproStima;
	}
	public void setDtApproStima(Date dtApproStima) {
		this.dtApproStima = dtApproStima;
	}
	public Date getDtRichiesta() {
		return dtRichiesta;
	}
	public void setDtRichiesta(Date dtRichiesta) {
		this.dtRichiesta = dtRichiesta;
	}
	public Date getDtInvioStima() {
		return dtInvioStima;
	}
	public void setDtInvioStima(Date dtInvioStima) {
		this.dtInvioStima = dtInvioStima;
	}
	public Date getDtDocTecnica() {
		return dtDocTecnica;
	}
	public void setDtDocTecnica(Date dtDocTecnica) {
		this.dtDocTecnica = dtDocTecnica;
	}
	public Date getDtInoltroOfferta() {
		return dtInoltroOfferta;
	}
	public void setDtInoltroOfferta(Date dtInoltroOfferta) {
		this.dtInoltroOfferta = dtInoltroOfferta;
	}
	public Date getDtAvvioAttivita() {
		return dtAvvioAttivita;
	}
	public void setDtAvvioAttivita(Date dtAvvioAttivita) {
		this.dtAvvioAttivita = dtAvvioAttivita;
	}
	public Date getDtStartSvil() {
		return dtStartSvil;
	}
	public void setDtStartSvil(Date dtStartSvil) {
		this.dtStartSvil = dtStartSvil;
	}
	public Date getDtSystem() {
		return dtSystem;
	}
	public void setDtSystem(Date dtSystem) {
		this.dtSystem = dtSystem;
	}
	public Date getDtProd() {
		return dtProd;
	}
	public void setDtProd(Date dtProd) {
		this.dtProd = dtProd;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public Integer getStimaGGUISP() {
		return stimaGGUISP;
	}
	public void setStimaGGUISP(Integer stimaGGUISP) {
		this.stimaGGUISP = stimaGGUISP;
	}
	public Float getGgUtilizzateISP() {
		return ggUtilizzateISP;
	}
	public void setGgUtilizzateISP(Float ggUtilizzateISP) {
		this.ggUtilizzateISP = ggUtilizzateISP;
	}
	public Float getResiduoISP() {
		return residuoISP;
	}
	public void setResiduoISP(Float residuoISP) {
		this.residuoISP = residuoISP;
	}
	public Integer getStimaKISP() {
		return stimaKISP;
	}
	public void setStimaKISP(Integer stimaKISP) {
		this.stimaKISP = stimaKISP;
	}
	public Integer getStimaKISPNoIVA() {
		return stimaKISPNoIVA;
	}
	public void setStimaKISPNoIVA(Integer stimaKISPNoIVA) {
		this.stimaKISPNoIVA = stimaKISPNoIVA;
	}
	public Integer getGgUtiliNNTD() {
		return ggUtiliNNTD;
	}
	public void setGgUtiliNNTD(Integer ggUtiliNNTD) {
		this.ggUtiliNNTD = ggUtiliNNTD;
	}
	public Integer getOreNTTDUtilizzate() {
		return oreNTTDUtilizzate;
	}
	public void setOreNTTDUtilizzate(Integer oreNTTDUtilizzate) {
		this.oreNTTDUtilizzate = oreNTTDUtilizzate;
	}
	public Integer getOreUtiliNTTD() {
		return oreUtiliNTTD;
	}
	public void setOreUtiliNTTD(Integer oreUtiliNTTD) {
		this.oreUtiliNTTD = oreUtiliNTTD;
	}
	public Integer getGgUtilizzatiNTTD() {
		return ggUtilizzatiNTTD;
	}
	public void setGgUtilizzatiNTTD(Integer ggUtilizzatiNTTD) {
		this.ggUtilizzatiNTTD = ggUtilizzatiNTTD;
	}
	public Integer getResiduoGGISP() {
		return residuoGGISP;
	}
	public void setResiduoGGISP(Integer residuoGGISP) {
		this.residuoGGISP = residuoGGISP;
	}
	public Integer getResiduoGGNTTD() {
		return residuoGGNTTD;
	}
	public void setResiduoGGNTTD(Integer residuoGGNTTD) {
		this.residuoGGNTTD = residuoGGNTTD;
	}
	public Float getTariffaFerNTTD() {
		return tariffaFerNTTD;
	}
	public void setTariffaFerNTTD(Float tariffaFerNTTD) {
		this.tariffaFerNTTD = tariffaFerNTTD;
	}
	
	public UserPureDTO getRefRealizzazNTTD() {
		return refRealizzazNTTD;
	}
	public void setRefRealizzazNTTD(UserPureDTO refRealizzazNTTD) {
		this.refRealizzazNTTD = refRealizzazNTTD;
	}
	public Integer getBasketOreResiduo() {
		return basketOreResiduo;
	}
	public void setBasketOreResiduo(Integer basketOreResiduo) {
		this.basketOreResiduo = basketOreResiduo;
	}
	public Integer getBasketGGResiduo() {
		return basketGGResiduo;
	}
	public void setBasketGGResiduo(Integer basketGGResiduo) {
		this.basketGGResiduo = basketGGResiduo;
	}
	public Integer getBasketISPGGResiduo() {
		return basketISPGGResiduo;
	}
	public void setBasketISPGGResiduo(Integer basketISPGGResiduo) {
		this.basketISPGGResiduo = basketISPGGResiduo;
	}
	public Integer getSottrarreATesoretto() {
		return sottrarreATesoretto;
	}
	public void setSottrarreATesoretto(Integer sottrarreATesoretto) {
		this.sottrarreATesoretto = sottrarreATesoretto;
	}
	public Integer getDispTesoResiduoDaAut() {
		return dispTesoResiduoDaAut;
	}
	public void setDispTesoResiduoDaAut(Integer dispTesoResiduoDaAut) {
		this.dispTesoResiduoDaAut = dispTesoResiduoDaAut;
	}
	public Integer getDispTesoResiduoAut() {
		return dispTesoResiduoAut;
	}
	public void setDispTesoResiduoAut(Integer dispTesoResiduoAut) {
		this.dispTesoResiduoAut = dispTesoResiduoAut;
	}
	public Integer getGgTesoTot() {
		return ggTesoTot;
	}
	public void setGgTesoTot(Integer ggTesoTot) {
		this.ggTesoTot = ggTesoTot;
	}
	@Override
	public String toString() {
		return "EvolutivaDTO [id=" + id + ", acronimo=" + acronimo + ", richiedenteISP=" + richiedenteISP + ", refNTTD="
				+ refNTTD + ", descrizione=" + descrizione + ", evolutivaReleasePROD=" + evolutivaReleasePROD
				+ ", tipoRichiesta=" + tipoRichiesta + ", loiNumOrdineNTTD=" + loiNumOrdineNTTD + ", statoTesorettoEvo="
				+ statoTesorettoEvo + ", rdaISP=" + rdaISP + ", tecSviluppo=" + tecSviluppo + ", dtApproStima="
				+ dtApproStima + ", dtRichiesta=" + dtRichiesta + ", dtInvioStima=" + dtInvioStima + ", dtDocTecnica="
				+ dtDocTecnica + ", dtInoltroOfferta=" + dtInoltroOfferta + ", dtAvvioAttivita=" + dtAvvioAttivita
				+ ", dtStartSvil=" + dtStartSvil + ", dtSystem=" + dtSystem + ", dtProd=" + dtProd + ", stato=" + stato
				+ ", stimaGGUISP=" + stimaGGUISP + ", ggUtilizzateISP=" + ggUtilizzateISP + ", residuoISP=" + residuoISP
				+ ", stimaKISP=" + stimaKISP + ", stimaKISPNoIVA=" + stimaKISPNoIVA + ", ggUtiliNNTD=" + ggUtiliNNTD
				+ ", oreNTTDUtilizzate=" + oreNTTDUtilizzate + ", oreUtiliNTTD=" + oreUtiliNTTD + ", ggUtilizzatiNTTD="
				+ ggUtilizzatiNTTD + ", residuoGGISP=" + residuoGGISP + ", residuoGGNTTD=" + residuoGGNTTD
				+ ", tariffaFerNTTD=" + tariffaFerNTTD + ", refRealizzazNTTD=" + refRealizzazNTTD
				+ ", basketOreResiduo=" + basketOreResiduo + ", basketGGResiduo=" + basketGGResiduo
				+ ", basketISPGGResiduo=" + basketISPGGResiduo + ", sottrarreATesoretto=" + sottrarreATesoretto
				+ ", dispTesoResiduoDaAut=" + dispTesoResiduoDaAut + ", dispTesoResiduoAut=" + dispTesoResiduoAut
				+ ", ggTesoTot=" + ggTesoTot + "]";
	}
	
	
	
}

