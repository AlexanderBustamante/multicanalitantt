package it.ntt.multicanalita.dto.pure;

import java.util.Date;

public class FineGiornataPureDTO {
	private Long id;
	private Float oreAm;
	private Float oreEvo;
	private Float oreExtra;
	private Float oreCorsi;
	private Float oreHandover;
	private Integer feedback;
	private Date giornata;
	
	private UserPureDTO user;
	
	
	public UserPureDTO getUser() {
		return user;
	}
	public void setUser(UserPureDTO user) {
		this.user = user;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Float getOreAm() {
		return oreAm;
	}
	public void setOreAm(Float oreAm) {
		this.oreAm = oreAm;
	}
	public Float getOreEvo() {
		return oreEvo;
	}
	public void setOreEvo(Float oreEvo) {
		this.oreEvo = oreEvo;
	}
	public Float getOreExtra() {
		return oreExtra;
	}
	public void setOreExtra(Float oreExtra) {
		this.oreExtra = oreExtra;
	}
	public Float getOreCorsi() {
		return oreCorsi;
	}
	public void setOreCorsi(Float oreCorsi) {
		this.oreCorsi = oreCorsi;
	}
	public Float getOreHandover() {
		return oreHandover;
	}
	public void setOreHandover(Float oreHandover) {
		this.oreHandover = oreHandover;
	}
	public Integer getFeedback() {
		return feedback;
	}
	public void setFeedback(Integer feedback) {
		this.feedback = feedback;
	}
	public Date getGiornata() {
		return giornata;
	}
	public void setGiornata(Date giornata) {
		this.giornata = giornata;
	}
	@Override
	public String toString() {
		return "FineGiornataPureDTO [id=" + id + ", oreAm=" + oreAm + ", oreEvo=" + oreEvo + ", oreExtra=" + oreExtra
				+ ", oreCorsi=" + oreCorsi + ", oreHandover=" + oreHandover + ", feedback=" + feedback + ", giornata="
				+ giornata + ", user=" + user + "]";
	}
	
	
	
}
