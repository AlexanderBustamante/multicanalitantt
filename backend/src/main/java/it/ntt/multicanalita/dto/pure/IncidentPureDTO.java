package it.ntt.multicanalita.dto.pure;

public class IncidentPureDTO {
	
	private Long id;
	private String  clusterAcronimo;
	private String  acronimo;
	private String	nIncident;
	private String requestType;
	private String esitoInt;
	private String tipoAss;
	private String gdlRiAss;
	private String note;
	private Boolean system;
	private String fte;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClusterAcronimo() {
		return clusterAcronimo;
	}

	public void setClusterAcronimo(String clusterAcronimo) {
		this.clusterAcronimo = clusterAcronimo;
	}

	public String getAcronimo() {
		return acronimo;
	}

	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}

	public String getnIncident() {
		return nIncident;
	}

	public void setnIncident(String nIncident) {
		this.nIncident = nIncident;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getEsitoInt() {
		return esitoInt;
	}

	public void setEsitoInt(String esitoInt) {
		this.esitoInt = esitoInt;
	}

	public String getTipoAss() {
		return tipoAss;
	}

	public void setTipoAss(String tipoAss) {
		this.tipoAss = tipoAss;
	}

	public String getGdlRiAss() {
		return gdlRiAss;
	}

	public void setGdlRiAss(String gdlRiAss) {
		this.gdlRiAss = gdlRiAss;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Boolean getSystem() {
		return system;
	}

	public void setSystem(Boolean system) {
		this.system = system;
	}

	public String getFte() {
		return fte;
	}

	@Override
	public String toString() {
		return "IncidentPureDTO [id=" + id + ", clusterAcronimo=" + clusterAcronimo + ", acronimo=" + acronimo
				+ ", nIncident=" + nIncident + ", requestType=" + requestType + ", esitoInt=" + esitoInt + ", tipoAss="
				+ tipoAss + ", gdlRiAss=" + gdlRiAss + ", note=" + note + ", system=" + system + ", fte=" + fte + "]";
	}

	public void setFte(String fte) {
		this.fte = fte;
	}

}
