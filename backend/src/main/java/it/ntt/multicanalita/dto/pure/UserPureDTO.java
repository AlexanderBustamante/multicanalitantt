package it.ntt.multicanalita.dto.pure;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sun.istack.NotNull;

import it.ntt.multicanalita.entity.UserEntity;

public class UserPureDTO {

	private Long id;
    private String nome;
    private String cognome;
    private String email; 
    private String ruolo;
    private String posizione;
    private Integer idCluApp;
    
//    @NotNull ESEMPI VALIDAZIONI
//    private String password;
//    @NotBlank 
//    private String name;
    //@Min(value = 18, message = "Age should not be less than 18")
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRuolo() {
		return ruolo;
	}
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	public String getPosizione() {
		return posizione;
	}
	public void setPosizione(String posizione) {
		this.posizione = posizione;
	}
	public Integer getIdCluApp() {
		return idCluApp;
	}
	public void setIdCluApp(Integer idCluApp) {
		this.idCluApp = idCluApp;
	}


}
