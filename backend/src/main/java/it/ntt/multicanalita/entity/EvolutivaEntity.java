package it.ntt.multicanalita.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Evolutiva")
public class EvolutivaEntity {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="acronimo", nullable=false, length=10)
	private String acronimo;
	@Column(name="richiedenteisp", nullable=false, length=250)
	private String richiedenteISP;
	@Column(name="refnttd", nullable=false, length=250)
	private String refNTTD;
	@Column(name="descrizione", nullable=false)
	private String descrizione;
	@Column(name="evolutivareleaseprod", nullable=false)
	private boolean evolutivaReleasePROD;
	@Column(name="tiporichiesta", nullable=true, length=250)
	private String tipoRichiesta;
	@Column(name="loinumordinenttd", nullable=true)
	private String loiNumOrdineNTTD;
	@Column(name="statotesorettoevo", nullable=true, length=250)
	private String statoTesorettoEvo;
	@Column(name="rdaisp", nullable=true, length=250)
	private String rdaISP;
	@Column(name="dtapprostima", nullable=true, length=250)
	private Date dtApproStima;
	@Column(name="dtrichiesta", nullable=true, length=250)
	private Date dtRichiesta;
	@Column(name="dtinviostima", nullable=true, length=250)
	private Date dtInvioStima;
	@Column(name="dtdoctecnica", nullable=true, length=250)
	private Date dtDocTecnica;
	@Column(name="dtinoltroofferta", nullable=true, length=250)
	private Date dtInoltroOfferta;
	@Column(name="dtavvioattivita", nullable=true, length=250)
	private Date dtAvvioAttivita;
	@Column(name="dtstartsvil", nullable=true, length=250)
	private Date dtStartSvil;
	@Column(name="dtsystem", nullable=true, length=250)
	private Date dtSystem;
	@Column(name="dtprod", nullable=true, length=250)
	private Date dtProd;
	@Column(name="stato", nullable=true, length=250)
	private String stato;
	@Column(name="stimagguisp", nullable=true)
	private Integer stimaGGUISP;
	@Column(name="ggutilizzateisp", nullable=true)
	private Float ggUtilizzateISP;
	@Column(name="residuoisp", nullable=true)
	private Float residuoISP;
	@Column(name="stimakisp", nullable=true)
	private Integer stimaKISP;
	@Column(name="stimakispnoiva", nullable=true)
	private Integer stimaKISPNoIVA;
	@Column(name="gguutilinntd", nullable=true)
	private Integer ggUtiliNNTD;
	@Column(name="orenttdutilizzate", nullable=true)
	private Integer oreNTTDUtilizzate;
	@Column(name="oreutilinttd", nullable=true)
	private Integer oreUtiliNTTD;
	@Column(name="ggutilizzatinttd", nullable=true)
	private Integer ggUtilizzatiNTTD;
	@Column(name="residuoggisp", nullable=true)
	private Integer residuoGGISP;
	@Column(name="residuoggnttd", nullable=true)
	private Integer residuoGGNTTD;
	@Column(name="tariffafernttd", nullable=true)
	private Float tariffaFerNTTD;
	
	@Column(name="basketoreresiduo", nullable=true)
	private Integer basketOreResiduo;
	@Column(name="basketggresiduo", nullable=true)
	private Integer basketGGResiduo;
	@Column(name="basketispggresiduo", nullable=true)
	private Integer basketISPGGResiduo;
	@Column(name="sottrarreatesoretto", nullable=true)
	private Integer sottrarreATesoretto;
	@Column(name="disptesoresiduodaaut", nullable=true)
	private Integer dispTesoResiduoDaAut;
	@Column(name="disptesoresiduoaut", nullable=true)
	private Integer dispTesoResiduoAut;
	@Column(name="ggtesotot", nullable=true)
	private Integer ggTesoTot;
	// QUESTE DUE DA CAMBIARE APPENA FACCIAMO RELAZIONE NEL DB
	@ManyToOne
	@JoinColumn(name = "refrealizzaznttd")
	private UserEntity refrealizzaznttd;
	
//	@Column(name="refrealizzaznttd", nullable=true, length=250)
//	private String refRealizzazNTTD;
	@Column(name="tecsviluppo", nullable=true, length=250)
	private String tecSviluppo;
	
	//@OneToMany(mappedBy = "evolutive", cascade = CascadeType.MERGE)
	
	@OneToMany(mappedBy = "evolutive", cascade = CascadeType.MERGE)
	private Set<EvolutivaFineGiornataEntity> evolutivaFineGiornata;
//	
	
//	public Set<EvolutivaFineGiornataEntity> getEvolutivaFineGiornata() {
//		return evolutivaFineGiornata;
//	}
//	public void setEvolutivaFineGiornata(Set<EvolutivaFineGiornataEntity> evolutivaFineGiornata) {
//		this.evolutivaFineGiornata = evolutivaFineGiornata;
//	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAcronimo() {
		return acronimo;
	}
	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}
	public String getRichiedenteISP() {
		return richiedenteISP;
	}
	public void setRichiedenteISP(String richiedenteISP) {
		this.richiedenteISP = richiedenteISP;
	}
	public String getRefNTTD() {
		return refNTTD;
	}
	public void setRefNTTD(String refNTTD) {
		this.refNTTD = refNTTD;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String rescrizione) {
		this.descrizione = rescrizione;
	}
	public boolean isEvolutivaReleasePROD() {
		return evolutivaReleasePROD;
	}
	public void setEvolutivaReleasePROD(boolean evolutivaReleasePROD) {
		this.evolutivaReleasePROD = evolutivaReleasePROD;
	}
	public String getTipoRichiesta() {
		return tipoRichiesta;
	}
	public void setTipoRichiesta(String tipoRichiesta) {
		this.tipoRichiesta = tipoRichiesta;
	}
	public String getLoiNumOrdineNTTD() {
		return loiNumOrdineNTTD;
	}
	public void setLoiNumOrdineNTTD(String loiNumOrdineNTTD) {
		this.loiNumOrdineNTTD = loiNumOrdineNTTD;
	}
	public String getStatoTesorettoEvo() {
		return statoTesorettoEvo;
	}
	public void setStatoTesorettoEvo(String statoTesorettoEvo) {
		this.statoTesorettoEvo = statoTesorettoEvo;
	}
	public String getRdaISP() {
		return rdaISP;
	}
	public void setRdaISP(String rdaISP) {
		this.rdaISP = rdaISP;
	}
	public Date getDtApproStima() {
		return dtApproStima;
	}
	public void setDtApproStima(Date dtApproStima) {
		this.dtApproStima = dtApproStima;
	}
	public Date getDtRichiesta() {
		return dtRichiesta;
	}
	public void setDtRichiesta(Date dtRichiesta) {
		this.dtRichiesta = dtRichiesta;
	}
	public Date getDtInvioStima() {
		return dtInvioStima;
	}
	public void setDtInvioStima(Date dtInvioStima) {
		this.dtInvioStima = dtInvioStima;
	}
	public Date getDtDocTecnica() {
		return dtDocTecnica;
	}
	public void setDtDocTecnica(Date dtDocTecnica) {
		this.dtDocTecnica = dtDocTecnica;
	}
	public Date getDtInoltroOfferta() {
		return dtInoltroOfferta;
	}
	public void setDtInoltroOfferta(Date dtInoltroOfferta) {
		this.dtInoltroOfferta = dtInoltroOfferta;
	}
	public Date getDtAvvioAttivita() {
		return dtAvvioAttivita;
	}
	public void setDtAvvioAttivita(Date dtAvvioAttivita) {
		this.dtAvvioAttivita = dtAvvioAttivita;
	}
	public Date getDtStartSvil() {
		return dtStartSvil;
	}
	public void setDtStartSvil(Date dtStartSvil) {
		this.dtStartSvil = dtStartSvil;
	}
	public Date getDtSystem() {
		return dtSystem;
	}
	public void setDtSystem(Date dtSystem) {
		this.dtSystem = dtSystem;
	}
	public Date getDtProd() {
		return dtProd;
	}
	public void setDtProd(Date dtProd) {
		this.dtProd = dtProd;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public Integer getStimaGGUISP() {
		return stimaGGUISP;
	}
	public void setStimaGGUISP(Integer stimaGGUISP) {
		this.stimaGGUISP = stimaGGUISP;
	}
	public Float getGgUtilizzateISP() {
		return ggUtilizzateISP;
	}
	public void setGgUtilizzateISP(Float ggUtilizzateISP) {
		this.ggUtilizzateISP = ggUtilizzateISP;
	}
	public Float getResiduoISP() {
		return residuoISP;
	}
	public void setResiduoISP(Float residuoISP) {
		this.residuoISP = residuoISP;
	}
	public Integer getStimaKISP() {
		return stimaKISP;
	}
	public void setStimaKISP(Integer stimaKISP) {
		this.stimaKISP = stimaKISP;
	}
	public Integer getStimaKISPNoIVA() {
		return stimaKISPNoIVA;
	}
	public void setStimaKISPNoIVA(Integer stimaKISPNoIVA) {
		this.stimaKISPNoIVA = stimaKISPNoIVA;
	}
	public Integer getGgUtiliNNTD() {
		return ggUtiliNNTD;
	}
	public void setGgUtiliNNTD(Integer ggUtiliNNTD) {
		this.ggUtiliNNTD = ggUtiliNNTD;
	}
	public Integer getOreNTTDUtilizzate() {
		return oreNTTDUtilizzate;
	}
	public void setOreNTTDUtilizzate(Integer oreNTTDUtilizzate) {
		this.oreNTTDUtilizzate = oreNTTDUtilizzate;
	}
	public Integer getOreUtiliNTTD() {
		return oreUtiliNTTD;
	}
	public void setOreUtiliNTTD(Integer oreUtiliNTTD) {
		this.oreUtiliNTTD = oreUtiliNTTD;
	}
	public Integer getGgUtilizzatiNTTD() {
		return ggUtilizzatiNTTD;
	}
	public void setGgUtilizzatiNTTD(Integer ggUtilizzatiNTTD) {
		this.ggUtilizzatiNTTD = ggUtilizzatiNTTD;
	}
	public Integer getResiduoGGISP() {
		return residuoGGISP;
	}
	public void setResiduoGGISP(Integer residuoGGISP) {
		this.residuoGGISP = residuoGGISP;
	}
	public Integer getResiduoGGNTTD() {
		return residuoGGNTTD;
	}
	public void setResiduoGGNTTD(Integer residuoGGNTTD) {
		this.residuoGGNTTD = residuoGGNTTD;
	}
	public Float getTariffaFerNTTD() {
		return tariffaFerNTTD;
	}
	public void setTariffaFerNTTD(Float tariffaFerNTTD) {
		this.tariffaFerNTTD = tariffaFerNTTD;
	}
	public Integer getBasketOreResiduo() {
		return basketOreResiduo;
	}
	public void setBasketOreResiduo(Integer basketOreResiduo) {
		this.basketOreResiduo = basketOreResiduo;
	}
	public Integer getBasketGGResiduo() {
		return basketGGResiduo;
	}
	public void setBasketGGResiduo(Integer basketGGResiduo) {
		this.basketGGResiduo = basketGGResiduo;
	}
	public Integer getBasketISPGGResiduo() {
		return basketISPGGResiduo;
	}
	public void setBasketISPGGResiduo(Integer basketISPGGResiduo) {
		this.basketISPGGResiduo = basketISPGGResiduo;
	}
	public Integer getSottrarreATesoretto() {
		return sottrarreATesoretto;
	}
	public void setSottrarreATesoretto(Integer sottrarreATesoretto) {
		this.sottrarreATesoretto = sottrarreATesoretto;
	}
	public Integer getDispTesoResiduoDaAut() {
		return dispTesoResiduoDaAut;
	}
	public void setDispTesoResiduoDaAut(Integer dispTesoResiduoDaAut) {
		this.dispTesoResiduoDaAut = dispTesoResiduoDaAut;
	}
	public Integer getDispTesoResiduoAut() {
		return dispTesoResiduoAut;
	}
	public void setDispTesoResiduoAut(Integer dispTesoResiduoAut) {
		this.dispTesoResiduoAut = dispTesoResiduoAut;
	}
	public Integer getGgTesoTot() {
		return ggTesoTot;
	}
	public void setGgTesoTot(Integer ggTesoTot) {
		this.ggTesoTot = ggTesoTot;
	}
	
	public UserEntity getRefrealizzaznttd() {
		return refrealizzaznttd;
	}
	public void setRefrealizzaznttd(UserEntity refrealizzaznttd) {
		this.refrealizzaznttd = refrealizzaznttd;
	}
	public String getTecSviluppo() {
		return tecSviluppo;
	}
	public void setTecSviluppo(String tecSviluppo) {
		this.tecSviluppo = tecSviluppo;
	}
	@Override
	public String toString() {
		return "EvolutivaEntity [id=" + id + ", acronimo=" + acronimo + ", richiedenteISP=" + richiedenteISP
				+ ", refNTTD=" + refNTTD + ", descrizione=" + descrizione + ", evolutivaReleasePROD="
				+ evolutivaReleasePROD + ", tipoRichiesta=" + tipoRichiesta + ", loiNumOrdineNTTD=" + loiNumOrdineNTTD
				+ ", statoTesorettoEvo=" + statoTesorettoEvo + ", rdaISP=" + rdaISP + ", dtApproStima=" + dtApproStima
				+ ", dtRichiesta=" + dtRichiesta + ", dtInvioStima=" + dtInvioStima + ", dtDocTecnica=" + dtDocTecnica
				+ ", dtInoltroOfferta=" + dtInoltroOfferta + ", dtAvvioAttivita=" + dtAvvioAttivita + ", dtStartSvil="
				+ dtStartSvil + ", dtSystem=" + dtSystem + ", dtProd=" + dtProd + ", stato=" + stato + ", stimaGGUISP="
				+ stimaGGUISP + ", ggUtilizzateISP=" + ggUtilizzateISP + ", residuoISP=" + residuoISP + ", stimaKISP="
				+ stimaKISP + ", stimaKISPNoIVA=" + stimaKISPNoIVA + ", ggUtiliNNTD=" + ggUtiliNNTD
				+ ", oreNTTDUtilizzate=" + oreNTTDUtilizzate + ", oreUtiliNTTD=" + oreUtiliNTTD + ", ggUtilizzatiNTTD="
				+ ggUtilizzatiNTTD + ", residuoGGISP=" + residuoGGISP + ", residuoGGNTTD=" + residuoGGNTTD
				+ ", tariffaFerNTTD=" + tariffaFerNTTD + ", basketOreResiduo=" + basketOreResiduo + ", basketGGResiduo="
				+ basketGGResiduo + ", basketISPGGResiduo=" + basketISPGGResiduo + ", sottrarreATesoretto="
				+ sottrarreATesoretto + ", dispTesoResiduoDaAut=" + dispTesoResiduoDaAut + ", dispTesoResiduoAut="
				+ dispTesoResiduoAut + ", ggTesoTot=" + ggTesoTot + ", refrealizzaznttd=" + refrealizzaznttd
				+ ", tecSviluppo=" + tecSviluppo + "]";
	}
	
	


	
	

}
