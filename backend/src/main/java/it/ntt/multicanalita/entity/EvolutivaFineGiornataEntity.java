package it.ntt.multicanalita.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import it.ntt.multicanalita.pk.EvolutivaFineGiornataPK;

@Entity
@Table(name="evolutiva_finegiornata")
public class EvolutivaFineGiornataEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//	@EmbeddedId
//	EvolutivaFineGiornataPK id;

	@Column(name="oreevo")
	private Float oreEvo; 
	
	@ManyToOne(cascade = CascadeType.MERGE)
	
//	@ManyToOne
	@JoinColumn(name="idevo")
//	@MapsId("id_evo")
	private EvolutivaEntity evolutive;
//	
	@ManyToOne(cascade = CascadeType.MERGE)
//	@ManyToOne
	@JoinColumn(name="idfinegiornata")
//	@MapsId("id_Fine_Giornata")
	private FineGiornataEntity fineGiornata;

	

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getOreEvo() {
		return oreEvo;
	}

	public void setOreEvo(Float oreEvo) {
		this.oreEvo = oreEvo;
	}

	public EvolutivaEntity getEvolutive() {
		return evolutive;
	}

	public void setEvolutive(EvolutivaEntity evolutive) {
		this.evolutive = evolutive;
	}

	public FineGiornataEntity getFineGiornata() {
		return fineGiornata;
	}

	public void setFineGiornata(FineGiornataEntity fineGiornata) {
		this.fineGiornata = fineGiornata;
	}
	
	
	
	

}
