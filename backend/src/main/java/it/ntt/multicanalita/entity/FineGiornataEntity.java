package it.ntt.multicanalita.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table (name = "finegiornata")
public class FineGiornataEntity {
	
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
		
		@Column(name="oream", nullable=false)
		private Float oreAM;
		@Column(name="oreevo", nullable=false)
		private Float oreEvo;
		@Column(name="oreextra", nullable=true)
		private Float oreExtra;
		@Column(name="orecorsi", nullable=true)
		private Float oreCorsi;
		@Column(name="orehandover", nullable=true)
		private Float oreHandover;
		@Column(name="feedback", nullable=true)
		private Integer feedback;
		public Float getOreAM() {
			return oreAM;
		}
		public void setOreAM(Float oreAM) {
			this.oreAM = oreAM;
		}
		public Float getOreEvo() {
			return oreEvo;
		}
		public void setOreEvo(Float oreEvo) {
			this.oreEvo = oreEvo;
		}
		public Float getOreExtra() {
			return oreExtra;
		}
		public void setOreExtra(Float oreExtra) {
			this.oreExtra = oreExtra;
		}
		public Float getOreCorsi() {
			return oreCorsi;
		}
		public void setOreCorsi(Float oreCorsi) {
			this.oreCorsi = oreCorsi;
		}
		public Float getOreHandover() {
			return oreHandover;
		}
		public void setOreHandover(Float oreHandover) {
			this.oreHandover = oreHandover;
		}

		@Column(name="giornata", nullable=true, length=250)
		private Date giornata;
		
		@ManyToOne
		@JoinColumn(name="iduser")
		private UserEntity utente;
		
		@ManyToMany(cascade = CascadeType.PERSIST)
		@JoinTable(
				name= "incident_finegiornata",
				joinColumns = {@JoinColumn(name = "idfinegiornata", insertable=true, updatable=true) },
				inverseJoinColumns = {@JoinColumn(name = "idinc")}
				
				)
		private List<IncidentEntity> incidentLavorati;
		
		@OneToMany(mappedBy = "fineGiornata", cascade = CascadeType.MERGE)
		private List<EvolutivaFineGiornataEntity> evolutivaFineGiornata;
		
				
		public List<EvolutivaFineGiornataEntity> getEvolutivaFineGiornata() {
			return evolutivaFineGiornata;
		}
		public void setEvolutivaFineGiornata(List<EvolutivaFineGiornataEntity> evolutivaFineGiornata) {
			this.evolutivaFineGiornata = evolutivaFineGiornata;
		}

		
		public List<IncidentEntity> getIncidentLavorati() {
			return incidentLavorati;
		}
		public void setIncidentLavorati(List<IncidentEntity> incidentLavorati) {
			this.incidentLavorati = incidentLavorati;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		
		public Integer getFeedback() {
			return feedback;
		}
		public void setFeedback(Integer feedback) {
			this.feedback = feedback;
		}
		public Date getGiornata() {
			return giornata;
		}
		public void setGiornata(Date giornata) {
			this.giornata = giornata;
		}
		public UserEntity getUtente() {
			return utente;
		}
		public void setUtente(UserEntity utente) {
			this.utente = utente;
		}
		
//		@Override
//		public String toString() {
//			return "FineGiornataEntity [id=" + id + ", oreAM=" + oreAM + ", oreEvo=" + oreEvo + ", oreExtra=" + oreExtra
//					+ ", oreCorsi=" + oreCorsi + ", oreHandover=" + oreHandover + ", feedback=" + feedback
//					+ ", giornata=" + giornata + ", utente=" + utente + ", incidentLavorati=" + incidentLavorati + "]";
//		}
		
		
		
		
}
