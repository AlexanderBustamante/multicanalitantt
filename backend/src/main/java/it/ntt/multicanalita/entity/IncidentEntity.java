package it.ntt.multicanalita.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table (name = "incident")
public class IncidentEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="clusteracronimo", nullable=false, length=40)
	private String clusterAcronimo;
	@Column(name="acronimo", nullable=false, length=20)
	private String acronimo;

	@Column(name="nincident", nullable=false, length=20)
	private String nIncident;

	@Column(name="requesttype", nullable=false, length=100)
	private String requestType;
	@Column(name="esitoint", nullable=false, length=150)
	private String esitoInt;
	@Column(name="tipoass", nullable=false, length=100)
	private String tipoAss;
	@Column(name="gdlriass", nullable=true, length=250)
	private String gdlRiAss;
	@Column(name="note", nullable=true)
	private String note;
	@Column(name="system", nullable=false)
	private Boolean system;
	@Column(name="fte", nullable=true, length=250)
	private String fte;
	
	@ManyToMany(mappedBy="incidentLavorati", cascade = CascadeType.PERSIST)
	private List<FineGiornataEntity> fineGiornata;
	
	public List<FineGiornataEntity> getFineGiornata() {
		return fineGiornata;
	}
	public void setFineGiornata(List<FineGiornataEntity> fineGiornata) {
		this.fineGiornata = fineGiornata;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getClusterAcronimo() {
		return clusterAcronimo;
	}
	public void setClusterAcronimo(String clusterAcronimo) {
		this.clusterAcronimo = clusterAcronimo;
	}
	public String getAcronimo() {
		return acronimo;
	}
	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}
	public String getnIncident() {
		return nIncident;
	}
	public void setnIncident(String nIncident) {
		this.nIncident = nIncident;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getEsitoInt() {
		return esitoInt;
	}
	public void setEsitoInt(String esitoInt) {
		this.esitoInt = esitoInt;
	}
	public String getTipoAss() {
		return tipoAss;
	}
	public void setTipoAss(String tipoAss) {
		this.tipoAss = tipoAss;
	}
	public String getGdlRiAss() {
		return gdlRiAss;
	}
	public void setGdlRiAss(String gdlRiAss) {
		this.gdlRiAss = gdlRiAss;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Boolean getSystem() {
		return system;
	}
	public void setSystem(Boolean system) {
		this.system = system;
	}
	public String getFte() {
		return fte;
	}
	public void setFte(String fte) {
		this.fte = fte;
	}
	@Override
	public String toString() {
		return "IncidentEntity [id=" + id + ", clusterAcronimo=" + clusterAcronimo + ", acronimo=" + acronimo
				+ ", nIncident=" + nIncident + ", requestType=" + requestType + ", esitoInt=" + esitoInt + ", tipoAss="
				+ tipoAss + ", gdlRiAss=" + gdlRiAss + ", note=" + note + ", system=" + system + ", fte=" + fte
				+ ", fineGiornata=" + fineGiornata + "]";
	}
	
}

