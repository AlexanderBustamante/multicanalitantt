package it.ntt.multicanalita.exceptions;

import java.util.Map;
import org.springframework.http.HttpStatus;

public interface IMulticanalitaDomainException {

    String getCode();
    
    HttpStatus getResponseStatus();
    
    Map<String, Object> getAdditionalInfo();
}
