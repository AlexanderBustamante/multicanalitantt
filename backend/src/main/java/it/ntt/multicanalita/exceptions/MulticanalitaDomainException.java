package it.ntt.multicanalita.exceptions;

import org.springframework.http.HttpStatus;
import java.util.Map;

public class MulticanalitaDomainException  extends Exception implements IMulticanalitaDomainException {

	private static final long serialVersionUID = -7106410725106229997L;

	/**
     * error code
     */
    private final String code;

    /**
     * status of the http response (default 500)
     */
    private final HttpStatus responseStatus;

    /**
     * error type (default TECHNICAL)
     */
    private final MulticanalitaErrorTypeEnum errorType;

    /**
     * error severity (default ERROR)
     */
    private final MulticanalitaSeverityEnum severity;

    /**
     * map of custom domain information
     */
    private final Map<String,Object> additionalInfo;



    public MulticanalitaDomainException(String message, String code, HttpStatus responseStatus) {
        super(message);
        this.code = code;
        this.responseStatus = responseStatus;
        this.errorType = MulticanalitaErrorTypeEnum.BUSINESS;
        this.severity= MulticanalitaSeverityEnum.ERROR;
        this.additionalInfo = null;
    }

    public MulticanalitaDomainException(String message, String code, HttpStatus responseStatus, MulticanalitaErrorTypeEnum errorType, MulticanalitaSeverityEnum severity) {
        super(message);
        this.code = code;
        this.responseStatus = responseStatus;
        this.errorType = errorType;
        this.severity = severity;
        this.additionalInfo = null;
    }

    public MulticanalitaDomainException(String message, String code, HttpStatus responseStatus, MulticanalitaErrorTypeEnum errorType, MulticanalitaSeverityEnum severity, Map<String, Object> additionalInfo) {
        super(message);
        this.code = code;
        this.responseStatus = responseStatus;
        this.errorType = errorType;
        this.severity = severity;
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public HttpStatus getResponseStatus() {
        return responseStatus;
    }

    public MulticanalitaErrorTypeEnum getErrorType() {
        return errorType;
    }

    public MulticanalitaSeverityEnum getSeverity() {
        return severity;
    }

    @Override
    public Map<String, Object> getAdditionalInfo() {
        return additionalInfo;
    }
}
