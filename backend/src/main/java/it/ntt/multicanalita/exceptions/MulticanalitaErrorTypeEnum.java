package it.ntt.multicanalita.exceptions;

public enum MulticanalitaErrorTypeEnum {
    TECHNICAL, BUSINESS
}
