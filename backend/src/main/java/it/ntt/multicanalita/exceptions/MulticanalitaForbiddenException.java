package it.ntt.multicanalita.exceptions;

import org.springframework.http.HttpStatus;

public class MulticanalitaForbiddenException extends MulticanalitaDomainException {

	private static final long serialVersionUID = -6106500527886476791L;

	public MulticanalitaForbiddenException(String message) {
        this(message, MulticanalitaErrorCode.CODE_GENERIC);
    }

    public MulticanalitaForbiddenException(String message, String code) {
        super(message, code, HttpStatus.FORBIDDEN);
    }

}
