package it.ntt.multicanalita.exceptions;

public enum MulticanalitaSeverityEnum {
    FATAL, ERROR, WARNING, INFO
}
