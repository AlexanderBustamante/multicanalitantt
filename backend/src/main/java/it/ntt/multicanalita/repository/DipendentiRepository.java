package it.ntt.multicanalita.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ntt.multicanalita.entity.DipendentiEntity;

@Repository
public interface DipendentiRepository extends JpaRepository<DipendentiEntity, Long> {
	DipendentiEntity findByNome(String nome);
	
}
