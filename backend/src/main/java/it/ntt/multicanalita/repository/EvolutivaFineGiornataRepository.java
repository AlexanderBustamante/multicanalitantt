package it.ntt.multicanalita.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ntt.multicanalita.entity.EvolutivaFineGiornataEntity;

@Repository
public interface EvolutivaFineGiornataRepository extends JpaRepository<EvolutivaFineGiornataEntity, Long> {
	

}
