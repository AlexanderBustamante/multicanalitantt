package it.ntt.multicanalita.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ntt.multicanalita.entity.EvolutivaEntity;
@Repository
public interface EvolutivaRepository extends JpaRepository<EvolutivaEntity, Long>{
	
}
