package it.ntt.multicanalita.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ntt.multicanalita.entity.FineGiornataEntity;
@Repository
public interface FineGiornataRepository extends JpaRepository<FineGiornataEntity, Long> {

}
