package it.ntt.multicanalita.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ntt.multicanalita.entity.EvolutivaFineGiornataEntity;
import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.repository.EvolutivaFineGiornataRepository;
import it.ntt.multicanalita.repository.FineGiornataRepository;

@Service
public class EvolutivaFineGiornataService extends BaseService {
	
	@Autowired
	EvolutivaFineGiornataRepository repository;
	
	
public EvolutivaFineGiornataEntity insert(EvolutivaFineGiornataEntity varEvolutivaFineGiornata) throws MulticanalitaDomainException {
		
		logger.info("Tabella di appoggio EvolutivaFineGiornataEntity da inserire: {}, ", varEvolutivaFineGiornata.toString());
		
		EvolutivaFineGiornataEntity esitoInsert = repository.save(varEvolutivaFineGiornata);
		logger.info("Tabella di appoggio EvolutivaFineGiornataEntity Insert:{} completata ", esitoInsert.toString());
		
		return esitoInsert;
	}

}
