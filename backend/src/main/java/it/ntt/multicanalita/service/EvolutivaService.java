package it.ntt.multicanalita.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.ntt.multicanalita.entity.EvolutivaEntity;
//import it.ntt.multicanalita.entity.EvolutiveEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
//import it.ntt.multicanalita.repository.EvolutiveRepository;
import it.ntt.multicanalita.repository.EvolutivaRepository;

@Service
public class EvolutivaService extends BaseService{
	@Autowired
	EvolutivaRepository repository;
	
//	@Autowired
//	EvolutiveRepository repository;
	
	public List<EvolutivaEntity> getEvolutive(){
		logger.info("Get evolutive");
		List<EvolutivaEntity> evolutive = repository.findAll();
		
		logger.info("FIND all completato con successso ");
		if(evolutive.size() >0) {
			return evolutive;
		}
		else {
			return new ArrayList<EvolutivaEntity>();
		}
	}
	
	public EvolutivaEntity getEvolutivaById(Long id) throws MulticanalitaDomainException{
		Optional<EvolutivaEntity> evolutiva =repository.findById(id);
		if(evolutiva.isPresent()) {
			return evolutiva.get();
		}
		else {
			throw new MulticanalitaDomainException("message Errore", "code", HttpStatus.BAD_REQUEST);
		}
	}
	
	public EvolutivaEntity insert(EvolutivaEntity varEvolutiva) throws MulticanalitaDomainException {
		System.out.println(varEvolutiva.toString());
		EvolutivaEntity esitoInsert = repository.save(varEvolutiva);
		logger.info("Evolutiva Insert:{} completata ",esitoInsert.toString());
		
		return esitoInsert;
	}
	
	public boolean deleteEvolutivaById(Long id) throws MulticanalitaDomainException {
		Optional<EvolutivaEntity> evolutiva = repository.findById(id);
		
		if (evolutiva.isPresent()) {
				repository.deleteById(id);
				logger.info("Evolutiva Delete id:{} completata con successo",id);
				return true;
			}
		else {
				logger.warn("Impossibile eliminare evolutiva non presente");
				return false;
		}
	}
	
	public EvolutivaEntity update(EvolutivaEntity evolutiva) throws MulticanalitaDomainException {
		Optional<EvolutivaEntity> evolutivaTemp = repository.findById(evolutiva.getId());
		if(evolutivaTemp.isPresent()) {
		  	EvolutivaEntity newEvolutiva = evolutivaTemp.get();
		  	newEvolutiva.setAcronimo(evolutiva.getAcronimo());
		  	newEvolutiva.setRichiedenteISP(evolutiva.getRichiedenteISP());
		  	newEvolutiva.setRefNTTD(evolutiva.getRefNTTD());
		  	newEvolutiva.setDescrizione(evolutiva.getDescrizione());
		  	newEvolutiva.setEvolutivaReleasePROD(evolutiva.isEvolutivaReleasePROD());
		  	newEvolutiva.setTipoRichiesta(evolutiva.getTipoRichiesta());
			newEvolutiva.setLoiNumOrdineNTTD(evolutiva.getLoiNumOrdineNTTD());
			newEvolutiva.setStatoTesorettoEvo(evolutiva.getStatoTesorettoEvo());
			newEvolutiva.setRdaISP(evolutiva.getRdaISP());
			newEvolutiva.setDtApproStima(evolutiva.getDtApproStima());
			newEvolutiva.setDtRichiesta(evolutiva.getDtRichiesta());
			newEvolutiva.setDtInvioStima(evolutiva.getDtApproStima());
			newEvolutiva.setDtDocTecnica(evolutiva.getDtDocTecnica());
			newEvolutiva.setDtInoltroOfferta(evolutiva.getDtInoltroOfferta());
			newEvolutiva.setDtAvvioAttivita(evolutiva.getDtAvvioAttivita());
			newEvolutiva.setDtStartSvil(evolutiva.getDtStartSvil());
			newEvolutiva.setDtSystem(evolutiva.getDtSystem());
			newEvolutiva.setDtProd(evolutiva.getDtProd());
			newEvolutiva.setStato(evolutiva.getStato());
			newEvolutiva.setStimaGGUISP(evolutiva.getStimaGGUISP());
			newEvolutiva.setGgUtilizzateISP(evolutiva.getGgUtilizzateISP());
			newEvolutiva.setResiduoISP(evolutiva.getResiduoISP());
			newEvolutiva.setStimaKISP(evolutiva.getStimaKISP());
			newEvolutiva.setStimaKISPNoIVA(evolutiva.getStimaKISPNoIVA());
			newEvolutiva.setGgUtiliNNTD(evolutiva.getGgUtiliNNTD());
			newEvolutiva.setOreNTTDUtilizzate(evolutiva.getOreNTTDUtilizzate());
			newEvolutiva.setOreUtiliNTTD(evolutiva.getOreUtiliNTTD());
			newEvolutiva.setGgUtilizzatiNTTD(evolutiva.getGgUtilizzatiNTTD());
			newEvolutiva.setResiduoGGISP(evolutiva.getResiduoGGISP());
			newEvolutiva.setResiduoGGNTTD(evolutiva.getResiduoGGNTTD());
			newEvolutiva.setTariffaFerNTTD(evolutiva.getTariffaFerNTTD());
			newEvolutiva.setBasketOreResiduo(evolutiva.getBasketOreResiduo());
			newEvolutiva.setBasketGGResiduo(evolutiva.getBasketGGResiduo());
			newEvolutiva.setBasketISPGGResiduo(evolutiva.getBasketISPGGResiduo());
			newEvolutiva.setSottrarreATesoretto(evolutiva.getSottrarreATesoretto());
			newEvolutiva.setDispTesoResiduoDaAut(evolutiva.getDispTesoResiduoDaAut());
			newEvolutiva.setDispTesoResiduoAut(evolutiva.getDispTesoResiduoAut());
			newEvolutiva.setGgTesoTot(evolutiva.getGgTesoTot());
			newEvolutiva.setRefrealizzaznttd(evolutiva.getRefrealizzaznttd());
			newEvolutiva.setTecSviluppo(evolutiva.getTecSviluppo());
	      
			newEvolutiva = this.repository.save(newEvolutiva);
	       
			return newEvolutiva;
		} 
		else {
			throw new MulticanalitaDomainException("Evolutiva non è presente", "code", HttpStatus.BAD_REQUEST);
		}
	}
}
