package it.ntt.multicanalita.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.repository.FineGiornataRepository;

@Service
public class FineGiornataService extends BaseService {
	@Autowired
	FineGiornataRepository repository;
	
	public List<FineGiornataEntity> getAllFineGiornata(){
		logger.info("getFinegiornata");
		List<FineGiornataEntity> fineGiornata = repository.findAll();
		logger.info("Fine giornata ricevuto da DB :{}  ",fineGiornata.toString());
		logger.info("size fine giornata = ", fineGiornata.size());
		if(fineGiornata.size() >0) {
			return fineGiornata;
		}
		else {
			return new ArrayList<FineGiornataEntity>();
		}
	}
	public FineGiornataEntity getById(Long id) throws MulticanalitaDomainException{
		logger.info("getFinegiornataById");
		Optional<FineGiornataEntity> fineGiornata = repository.findById(id);
		logger.info("Fine giornata ricevuto da DB :{}  ",fineGiornata.toString());
		if(fineGiornata.isPresent()) {
			return fineGiornata.get();
		}
		else {
			throw new MulticanalitaDomainException("message Errore", "code", HttpStatus.BAD_REQUEST);
		}
		
		
		
	}
	public boolean deleteFineGiornataById(Long id) throws MulticanalitaDomainException {
		Optional<FineGiornataEntity> endGiornata = repository.findById(id);
		
		if (endGiornata.isPresent()) {
				repository.deleteById(id);
				logger.info("FineGiornata Delete id:{} completata con successo",id);
				return true;
			}
		else {
				logger.warn("Impossibile eliminare : FineGiornata non presente");
				return false;
		}
	}

	public FineGiornataEntity update(FineGiornataEntity fineGiornata) throws MulticanalitaDomainException {
		Optional<FineGiornataEntity> fineGiornataTemp = repository.findById(fineGiornata.getId());
		if(fineGiornataTemp.isPresent()) {
			FineGiornataEntity newFineGiornata = fineGiornataTemp.get();
			
			newFineGiornata.setId(fineGiornata.getId());
			newFineGiornata.setOreAM(fineGiornata.getOreAM());
			newFineGiornata.setOreEvo(fineGiornata.getOreEvo());
			newFineGiornata.setOreExtra(fineGiornata.getOreExtra());
			newFineGiornata.setOreHandover(fineGiornata.getOreHandover());
			newFineGiornata.setFeedback(fineGiornata.getFeedback());
			newFineGiornata.setGiornata(fineGiornata.getGiornata());
//			newFineGiornata.setUser(userConverter.convertUserEntityToUserDTO(input.getUtente()));
//			newFineGiornata.setIncidentLavorati(incidentConverter.converterIncidentEntityToListIncidentDTO(input.getIncidentLavorati()));
//			newFineGiornata.setEvolutiveLavorate(evoFinConverter.converterEvolutivaEvolutivaFineGiornataEntityTOEvolutivaFineGiornataDTO(input.getEvolutivaFineGiornata())); 
			
	    
		  	newFineGiornata = this.repository.save(newFineGiornata);
	       
			return newFineGiornata;
		} 
		else {
			throw new MulticanalitaDomainException("Incident non è presente", "code", HttpStatus.BAD_REQUEST);
		}
	}
	
	public FineGiornataEntity insert(FineGiornataEntity varFineGiornata) throws MulticanalitaDomainException {
		
		logger.info("Fine giornata da inserire: {}, ", varFineGiornata.toString());
		
		FineGiornataEntity esitoInsert = repository.save(varFineGiornata);
		logger.info("Fine giornata Insert:{} completata ", esitoInsert.toString());
		
		return esitoInsert;
	}
}
