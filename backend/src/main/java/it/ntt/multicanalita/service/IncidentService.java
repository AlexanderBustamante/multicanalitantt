package it.ntt.multicanalita.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.ntt.multicanalita.entity.EvolutivaEntity;
import it.ntt.multicanalita.entity.FineGiornataEntity;
import it.ntt.multicanalita.entity.IncidentEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.repository.IncidentRepository;

@Service
public class IncidentService extends BaseService{
	@Autowired
	IncidentRepository repository;
	
	public List<IncidentEntity> getAllIncident(){
		logger.info("Get incident");
		List<IncidentEntity> incident = repository.findAll();
		
		logger.info("FIND all completato con successso ");
		if(incident.size() >0) {
			return incident;
		}
		else {
			return new ArrayList<IncidentEntity>();
		}
	}
	
	public List<IncidentEntity> insert(List<IncidentEntity> varIncident) throws MulticanalitaDomainException {
		
		System.out.println(varIncident.toString());
		
		List<IncidentEntity> esitoInsert = repository.saveAll(varIncident);
		logger.info("Incident Insert:{} completata ", esitoInsert.toString());
		
		return esitoInsert;
	}
	
	
	
	
	public boolean deleteIncidentById(Long id) throws MulticanalitaDomainException {
		Optional<IncidentEntity> incident = repository.findById(id);
		
		if (incident.isPresent()) {
				repository.deleteById(id);
				logger.info("Incident Delete id:{} completata con successo",id);
				return true;
			}
		else {
				logger.warn("Impossibile eliminare incident non presente");
				return false;
		}
	}
	public IncidentEntity getById(Long id) throws MulticanalitaDomainException{
		logger.info("getFinegiornataById");
		Optional<IncidentEntity> incident = repository.findById(id);
		logger.info("Incident get by id  ricevuto da DB :{}  ",incident.toString());
		if(incident.isPresent()) {
			return incident.get();
		}
		else {
			throw new MulticanalitaDomainException("message Errore", "code", HttpStatus.BAD_REQUEST);
		}
	}
	
	public IncidentEntity update(IncidentEntity incident) throws MulticanalitaDomainException {
		Optional<IncidentEntity> incidentTemp = repository.findById(incident.getId());
		if(incidentTemp.isPresent()) {
		  	IncidentEntity newIncident = incidentTemp.get();
		  	newIncident.setAcronimo(incident.getAcronimo());
		  	newIncident.setClusterAcronimo(incident.getClusterAcronimo());
		  	newIncident.setEsitoInt(incident.getEsitoInt());
		  	newIncident.setFte(incident.getFte());
		  	newIncident.setTipoAss(incident.getTipoAss());
		  	newIncident.setSystem(incident.getSystem());
		  	newIncident.setRequestType(incident.getRequestType());
		  	newIncident.setNote(incident.getNote());
		  	newIncident.setnIncident(incident.getnIncident());
		  	newIncident.setId(incident.getId());
		  	newIncident.setGdlRiAss(incident.getGdlRiAss());
	    
			newIncident = this.repository.save(newIncident);
	       
			return newIncident;
		} 
		else {
			throw new MulticanalitaDomainException("Incident non è presente", "code", HttpStatus.BAD_REQUEST);
		}
	}
}
