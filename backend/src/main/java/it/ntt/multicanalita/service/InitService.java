package it.ntt.multicanalita.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import it.ntt.multicanalita.dto.InputDTO;
import it.ntt.multicanalita.entity.DipendentiEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.repository.DipendentiRepository;

@Service 
public class InitService extends BaseService {
    @Autowired
    DipendentiRepository repository;
    
	public String callService(InputDTO input) {
		logger.debug("");
		return input.getInit();
	}
      
    public List<DipendentiEntity> getDipendenti() {
		logger.debug("getDipendenti");
        List<DipendentiEntity> dipendenti = repository.findAll();
        if(dipendenti.size() > 0) {
            return dipendenti;
        } else {
            return new ArrayList<DipendentiEntity>();
        }
    }
     
    public DipendentiEntity getDipendentiById(Long id) throws MulticanalitaDomainException {
        Optional<DipendentiEntity> dipendente = repository.findById(id);
        if(dipendente.isPresent()) {
            return dipendente.get();
        } else {
            throw new MulticanalitaDomainException("message Errore", "code", HttpStatus.BAD_REQUEST);
        }
    }
    
    public DipendentiEntity getDipendentiByNome(String nome) throws MulticanalitaDomainException {
        DipendentiEntity dipendente = repository.findByNome(nome);
        return dipendente;
    }
     
    public DipendentiEntity createOrUpdateDipendenti(DipendentiEntity entity) throws MulticanalitaDomainException {
        Optional<DipendentiEntity> dipendente = repository.findById(entity.getId());
        if(dipendente.isPresent()) {
            DipendentiEntity newEntity = dipendente.get();
            newEntity.setNome(entity.getNome());
            newEntity.setCognome(entity.getCognome());
            newEntity.setEmail(entity.getEmail()); 
            newEntity = repository.save(newEntity);
             
            return newEntity;
        } else {
            entity = repository.save(entity);            
            return entity;
        }
    }
     
    public void deleteDipendentiById(Long id) throws MulticanalitaDomainException {
        Optional<DipendentiEntity> dipendente = repository.findById(id);
        if(dipendente.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new MulticanalitaDomainException("message Errore", "code", HttpStatus.BAD_REQUEST);
        }
    }
}