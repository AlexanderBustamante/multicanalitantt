package it.ntt.multicanalita.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.ntt.multicanalita.entity.UserEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.repository.UserRepository;

@Service
public class UserService extends BaseService{
	
	@Autowired
	UserRepository repository;
	
	public List<UserEntity> getUsers(){
		logger.debug("getUsers");
		List<UserEntity> users = repository.findAll();
		logger.info("size user = ", users.size());
		if(users.size() >0) {
			return users;
		}
		else {
			return new ArrayList<UserEntity>();
		}
	}
	public UserEntity getUserById(Long id) throws MulticanalitaDomainException{
	 Optional<UserEntity> user = repository.findById(id);
		if(user.isPresent()){
			return user.get();
		}
		else {
			throw new MulticanalitaDomainException("message Errore", "code", HttpStatus.BAD_REQUEST);
		}
	}

	public Boolean deleteUserById(Long id) throws MulticanalitaDomainException {
		Optional<UserEntity> utenteProva=repository.findById(id);
		if (utenteProva.isPresent()) {
			repository.deleteById(id);
			return true;
		}
		else
			
			return false;
	}

	public UserEntity insert(UserEntity user) throws MulticanalitaDomainException {
//		System.out.println(user.toString());
//		boolean valido = true;
//		
//		if(user.getCognome().isBlank() || user.getEmail().isBlank() || user.getNome().isBlank())
//			valido = false;
//			System.out.println(valido);
//		if(user.getRuolo() != "admin" && user.getRuolo() != "dipendente" && user.getRuolo() != "esterno")
//			valido = false;
//		System.out.println(valido);
//		UserEntity esito = null;
//		if(valido)
		UserEntity	esito = repository.save(user);
	
		return esito;
	}
	/**
	 * Metodo che permette di modificare i campi del user.
	 * @param user è lo user con i nuovi campi.
	 * @return ritorna il nuovo user modificato.
	 * @throws MulticanalitaDomainException
	 */
	public UserEntity update(UserEntity user) throws MulticanalitaDomainException {
		Optional<UserEntity> userTemp = repository.findById(user.getId());
        if(userTemp.isPresent()) {
            UserEntity newUser = userTemp.get();
            newUser.setNome(user.getNome());
            newUser.setCognome(user.getCognome());
            newUser.setEmail(user.getEmail());
            newUser.setRuolo(user.getRuolo());
            newUser.setPosizione(user.getPosizione());
            newUser.setIdCluApp(user.getIdCluApp());
            
            
            newUser = repository.save(newUser);
             
            return newUser;
            
        } else {
        	throw new MulticanalitaDomainException("Utente non � presente", "code", HttpStatus.BAD_REQUEST);
        }
		
	}
}
