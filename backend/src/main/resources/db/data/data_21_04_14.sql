DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Dipendenti;

CREATE TABLE Dipendenti (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cognome VARCHAR(250) NOT NULL,
  email VARCHAR(250) DEFAULT NULL,
  pwd 	VARCHAR(50) NOT NULL,
  ruolo varchar(50) 
  posizione varchar(50)
);
INSERT INTO Dipendenti (nome, cognome, email) VALUES
  ('Andrea', 'Caria', 'AndreaMarco.Caria@nttdata.com'),
  ('Marisa', 'Mastroleo', 'Marisa.Mastroleo@nttdata.com'),
  ('Florian', 'Karici', 'Florian.Karici@nttdata.com'),
  ('Andrea', 'Luca', 'Andrea.Luca@nttdata.com')
  ;
CREATE TABLE User(
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cognome VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL UNIQUE,
  ruolo VARCHAR(50) NOT NULL CONSTRAINT verifica_ruolo CHECK (ruolo = 'admin' OR ruolo = 'dipendente' OR ruolo = 'esterno' ),
  posizione VARCHAR(50)  DEFAULT NULL,
  idCluApp INTEGER
);
INSERT INTO User(nome, cognome, email, ruolo, idCluAppa ) VALUES
  ('Andrea', 'Caria', 'AndreaMarco.Caria@nttdata.com','dipendente',1),
  ('Marisa', 'Mastroleo', 'Marisa.Mastroleo@nttdata.com','dipendente',1),
  ('Florian', 'Karici', 'Florian.Karici@nttdata.com','dipendente',2),
  ('Andrea', 'Luca', 'Andrea.Luca@nttdata.com','dipendente',3)
  ;