DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Dipendenti;
DROP TABLE IF EXISTS Evolutive;

CREATE TABLE Dipendenti (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cognome VARCHAR(250) NOT NULL,
  email VARCHAR(250) DEFAULT NULL
);
INSERT INTO Dipendenti (nome, cognome, email) VALUES
  ('Andrea', 'Caria', 'AndreaMarco.Caria@nttdata.com'),
  ('Marisa', 'Mastroleo', 'Marisa.Mastroleo@nttdata.com'),
  ('Florian', 'Karici', 'Florian.Karici@nttdata.com'),
  ('Andrea', 'Luca', 'Andrea.Luca@nttdata.com')
  ;
CREATE TABLE User(
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cognome VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL UNIQUE,
  ruolo VARCHAR(50) NOT NULL CONSTRAINT verifica_ruolo CHECK (ruolo = 'admin' OR ruolo = 'dipendente' OR ruolo = 'esterno' ),
  posizione VARCHAR(50)  DEFAULT NULL,
  idcluapp INTEGER
);
INSERT INTO User(nome, cognome, email, ruolo, idCluApp ) VALUES
  ('Andrea', 'Caria', 'AndreaMarco.Caria@nttdata.com','dipendente',1),
  ('Marisa', 'Mastroleo', 'Marisa.Mastroleo@nttdata.com','dipendente',1),
  ('Florian', 'Karici', 'Florian.Karici@nttdata.com','dipendente',2),
  ('Andrea', 'Luca', 'Andrea.Luca@nttdata.com','dipendente',3)
  ;
  
CREATE TABLE Evolutive(
	id INT AUTO_INCREMENT  PRIMARY KEY,
	acronimo VARCHAR(10) NOT NULL,
	RichiedenteISP VARCHAR(250) NOT NULL,
	RefNTTD VARCHAR(250) NOT NULL,
	Descrizione TEXT NOT NULL,
	EvolutivaReleasePROD BOOLEAN NOT NULL,
	TipoRichiesta VARCHAR(250),
	LoiNumOrdineNTTD TEXT,
	StatoTesorettoEvo VARCHAR(250),
	RdaISP VARCHAR(250),
	TecSviluppo VARCHAR(250),  -- scelta multipla
	DTApproStima DATE,
	DTRichieta DATE,
	DTInvioStima DATE,
	DTDocTecnica DATE,
	DTInoltroOfferta DATE,
	DTAvvioAttivita DATE,
	DTStartSvil DATE,
	DTSystem DATE,
	DTProd DATE,
	Stato VARCHAR(250),
	StimaGGUISP INTEGER,
	GGUtilizzateISP FLOAT,
	ResiduoISP FLOAT,
	StimaKISP INTEGER,
	StimaKISPNoIVA INTEGER,
	GGUutiliNNT INTEGER,
	OreNTTDUtilizzate INTEGER,
	OreUtiliNTTD INTEGER,
	GGUtilizzatiNTTD INTEGER,
	ResiduoGGISP INTEGER,
	ResiduoGGNTTD INTEGER,
	TariffaFerNTTD DOUBLE,
	RefRealizzazNTTD VARCHAR(250), -- selezione multipla
	BasketOreResiduo INTEGER,
	BasketGGResiduo INTEGER,
	BasketISPGGResiduo INTEGER,
	SottrarreATesoretto	INTEGER,
	DispTesoResiduoDaAut INTEGER,
	DispTesoResiduoAut INTEGER,
	GGTesoTot INTEGER
);