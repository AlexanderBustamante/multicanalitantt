DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Dipendenti;
DROP TABLE IF EXISTS Evolutiva;

CREATE TABLE Dipendenti (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cognome VARCHAR(250) NOT NULL,
  email VARCHAR(250) DEFAULT NULL
);
INSERT INTO Dipendenti (nome, cognome, email) VALUES
  ('Andrea', 'Caria', 'AndreaMarco.Caria@nttdata.com'),
  ('Marisa', 'Mastroleo', 'Marisa.Mastroleo@nttdata.com'),
  ('Florian', 'Karici', 'Florian.Karici@nttdata.com'),
  ('Andrea', 'Luca', 'Andrea.Luca@nttdata.com')
  ;
CREATE TABLE User(
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cognome VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL UNIQUE,
  ruolo VARCHAR(50) NOT NULL CONSTRAINT verifica_ruolo CHECK (ruolo = 'admin' OR ruolo = 'dipendente' OR ruolo = 'esterno' ),
  posizione VARCHAR(50)  DEFAULT NULL,
  idcluapp INTEGER
);
INSERT INTO User(nome, cognome, email, ruolo, idCluApp ) VALUES
  ('Andrea', 'Caria', 'AndreaMarco.Caria@nttdata.com','dipendente',1),
  ('Marisa', 'Mastroleo', 'Marisa.Mastroleo@nttdata.com','dipendente',1),
  ('Florian', 'Karici', 'Florian.Karici@nttdata.com','dipendente',2),
  ('Andrea', 'Luca', 'Andrea.Luca@nttdata.com','dipendente',3)
  ;
  
CREATE TABLE Evolutiva(
	id INT AUTO_INCREMENT  PRIMARY KEY,
	acronimo VARCHAR(10) NOT NULL,
	richiedenteisp VARCHAR(250) NOT NULL,
	refnttd VARCHAR(250) NOT NULL,
	descrizione TEXT, --not null da inserire(f.k commentato)
	evolutivareleaseprod BOOLEAN NOT NULL,
	tiporichiesta VARCHAR(250),
	loinumordinenttd TEXT,
	statotesorettoevo VARCHAR(250),
	rdaisp VARCHAR(250),
	dtapprostima DATE,
	dtrichiesta DATE,
	dtinviostima DATE,
	dtdoctecnica DATE,
	dtinoltroofferta DATE,
	dtavvioattivita DATE,
	dtstartsvil DATE,
	dtsystem DATE,
	dtprod DATE,
	stato VARCHAR(250),
	stimagguisp INTEGER,
	ggutilizzateisp FLOAT,
	residuoisp FLOAT,
	stimakisp INTEGER,
	stimakispnoiva INTEGER,
	gguutilinntd INTEGER,
	orenttdutilizzate INTEGER,
	oreutilinttd INTEGER,
	ggutilizzatinttd INTEGER,
	residuoggisp INTEGER,
	residuoggnttd INTEGER,
	tariffafernttd FLOAT,
	basketoreresiduo INTEGER,
	basketggresiduo INTEGER,
	basketispggresiduo INTEGER,
	sottrarreatesoretto	INTEGER,
	disptesoresiduodaaut INTEGER,
	disptesoresiduoaut INTEGER,
	ggtesotot INTEGER,
	refrealizzaznttd INTEGER FOREIGN KEY REFERENCES User(id), -- selezione multipla
	tecsviluppo VARCHAR(250),-- scelta multipla
	
);

INSERT INTO Evolutiva(acronimo, richiedenteisp,refrealizzaznttd, refnttd, descrizione, evolutivareleaseprod,tiporichiesta ) VALUES
('APDF0', 'Angelino', 'Giangualano',1,' Sono una evolutiva di solito evolvo', true,'EVOLUTIVA');

INSERT INTO Incident(clusterAcronimo, acronimo, nIncident, requestType, esitoInt, tipoAss, gdlRiAss, system)  values 
('MSAD/ISPAD',	'MFEA0',	'INC006212843', 'Service request',	'Analisi e riassegnazione',	'Riassegnazione',	'HDO NAPOLI2', false);



INSERT INTO FineGiornata(giornata, oreAM, oreEvo, idUser) values 	
('2021-12-10', 4, 4, 1);


INSERT INTO incident_fineGiornata(idInc, idFineGiornata ) values 
(1, 1);

insert into evolutiva_fineGiornata(idEvo, idFineGiornata, oreEvo ) values 
(1, 1, 4);	
	

--