package it.ntt.multicanalita;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.ntt.multicanalita.entity.DipendentiEntity;
import it.ntt.multicanalita.repository.DipendentiRepository;
import it.ntt.multicanalita.service.InitService;

@SpringBootTest
public class ApplicationTests {
    
    @InjectMocks // questo è quello che volete testare 
    InitService service;
     
    @Mock // questo e quello che mocki
    DipendentiRepository repository;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void getDipendentiTest() {
        List<DipendentiEntity> list = new ArrayList<DipendentiEntity>();

        DipendentiEntity uno = new DipendentiEntity();
        uno.setNome("nome");
        uno.setCognome("cognome");
        uno.setEmail("email");
        uno.setId(new Long(1)); 
        list.add(uno); 
         
        Mockito.when(this.repository.findAll()).thenReturn(list);
         
        List<DipendentiEntity> listService = this.service.getDipendenti(); 
         
        assertEquals(1, listService.size());
        assertEquals(list.get(0).getCognome(), "cognome");
    }
     

}


