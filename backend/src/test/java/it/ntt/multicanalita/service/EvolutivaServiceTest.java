package it.ntt.multicanalita.service;

import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
//import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import it.ntt.multicanalita.entity.EvolutivaEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.repository.EvolutivaRepository;

@SpringBootTest
public class EvolutivaServiceTest {
	@InjectMocks
	EvolutivaService service;
	
	@Mock
	EvolutivaRepository repository;
	
	@Before
	    public void init() {
	        MockitoAnnotations.initMocks(this);
	    }
	@Test
	public void deleteEvolutivaPresente() {
		EvolutivaEntity evolutiva = new EvolutivaEntity();
		evolutiva.setAcronimo("TEST");
		evolutiva.setRichiedenteISP("TEST");
		evolutiva.setRefNTTD("TEST");
		evolutiva.setDescrizione("TEST");
		evolutiva.setEvolutivaReleasePROD(false);
		evolutiva.setId(new Long(1));
		
		Optional<EvolutivaEntity> optionalEnt = Optional.of(evolutiva);
		Mockito.when(repository.findById(evolutiva.getId())).thenReturn(optionalEnt);
		boolean deleteTest = false;
		try {
			 deleteTest = service.deleteEvolutivaById(evolutiva.getId());
			
		} catch (MulticanalitaDomainException e) {
			e.printStackTrace();
		}
		assertTrue(deleteTest);
		
	}
	@Test
	public void deleteEvolutivaNonPresente() {
		EvolutivaEntity evoTest = new EvolutivaEntity();
		Optional<EvolutivaEntity> optionalEnt = Optional.empty();
		 
		Mockito.when(repository.findById(evoTest.getId())).thenReturn(optionalEnt);
		boolean deleteTest = false;
		try {
			 deleteTest = service.deleteEvolutivaById((long) 2);
			
		} catch (MulticanalitaDomainException e) {
			e.printStackTrace();
		}
		assertTrue(!deleteTest);
		
	}
	@Test
	public void insertEvolutivaTest() {
		EvolutivaEntity evolutiva = new EvolutivaEntity();
		evolutiva.setAcronimo("MFEA0");
		evolutiva.setRichiedenteISP("EDY");
		evolutiva.setRefNTTD("ALE");
		evolutiva.setDescrizione("GRAFICI JASPER");
		evolutiva.setEvolutivaReleasePROD(false);
		
		Mockito.when(repository.save(evolutiva)).thenReturn(evolutiva);
		
		EvolutivaEntity evolutivaTest = repository.save(evolutiva);
		assertEquals(evolutiva, evolutivaTest);
		
		
	}
	
	@Test
	public void getEvolutive(){
		List<EvolutivaEntity> listaEvo = new ArrayList<EvolutivaEntity>();

		EvolutivaEntity uno = new EvolutivaEntity();
        uno.setAcronimo("acronimo");
        uno.setRichiedenteISP("richiedenteISP");
        uno.setRefNTTD("ref");
        uno.setEvolutivaReleasePROD(true); 
        
		EvolutivaEntity due = new EvolutivaEntity();
        due.setAcronimo("acronimo");
        due.setRichiedenteISP("richiedenteISP");
        due.setRefNTTD("ref");
        due.setEvolutivaReleasePROD(true); 
        
        
        listaEvo.add(uno);
        listaEvo.add(due);
        
        Mockito.when(repository.findAll()).thenReturn(listaEvo);
        
        List<EvolutivaEntity> listaTest = service.getEvolutive();
        assertEquals(2, listaTest.size());
        assertEquals(listaEvo, listaTest);
		
	}
	
	@Test
	public void getEvolutivaById(){

		EvolutivaEntity test = (new EvolutivaEntity());
		test.setId((long) 1);
		test.setAcronimo("acronimo");
        test.setRichiedenteISP("richiedenteISP");
        test.setRefNTTD("ref");
        test.setEvolutivaReleasePROD(true); 
        Long idtry=(long) 1;

        
	    Optional<EvolutivaEntity> opt = Optional.of(test);
		
		Mockito.when(repository.findById(test.getId())).thenReturn(opt);
		EvolutivaEntity find=null;
		try {
			 find = service.getEvolutivaById(idtry);
			
		} catch (MulticanalitaDomainException e) {
			e.printStackTrace();
		}
		assertEquals(find, test);
	}

	@Test
	public void updateTest() {
		
		EvolutivaEntity newEvolutiva = new EvolutivaEntity();
		newEvolutiva.setAcronimo("APDF0");
		newEvolutiva.setDescrizione("test sulla modifica dell'evolutiva");
		newEvolutiva.setEvolutivaReleasePROD(false);
		//newEvolutiva.setRefRealizzazNTTD("Giangualano");
		//newEvolutiva.setRefRealizzazNTTD("Luca");
		
	    Optional<EvolutivaEntity> opt = Optional.of(newEvolutiva);
		
		Mockito.when(repository.findById(newEvolutiva.getId())).thenReturn(opt);
		Mockito.when(repository.save(newEvolutiva)).thenReturn(newEvolutiva);
		
		EvolutivaEntity evoTest = null;
		try {
			evoTest = service.update(newEvolutiva);
		} catch (MulticanalitaDomainException e) {
			e.printStackTrace();
		}
		
		assertEquals(newEvolutiva, evoTest);
		assertTrue(opt.isPresent());
	}
}
