package it.ntt.multicanalita.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import it.ntt.multicanalita.entity.UserEntity;
import it.ntt.multicanalita.exceptions.MulticanalitaDomainException;
import it.ntt.multicanalita.repository.UserRepository;


@SpringBootTest
public class UserServiceTest {
	
	@InjectMocks
	UserService service;
	
	@Mock
	UserRepository repository;
	
	@Before
	    public void init() {
	        MockitoAnnotations.initMocks(this);
	    }
	 
	@Test
	public void getUsers(){
		List<UserEntity> listaUser = new ArrayList<UserEntity>();

		UserEntity uno = new UserEntity();
        uno.setNome("nome");
        uno.setCognome("cognome");
        uno.setEmail("email");
        uno.setId(new Long(1)); 
        
        UserEntity due = new UserEntity();
        due.setNome("nome");
        due.setCognome("cognome");
        due.setEmail("email");
        due.setId(new Long(1)); 
        
        
        listaUser.add(uno);
        listaUser.add(due);
        
        Mockito.when(repository.findAll()).thenReturn(listaUser);
        
        List<UserEntity> listaTest = service.getUsers();
        assertEquals(2, listaTest.size());
        assertEquals(listaUser, listaTest);
		
	}
	@Test
	public void updateTest() {
		
		UserEntity newUser = new UserEntity();
		newUser.setNome("florian");
		newUser.setCognome("karici");
		newUser.setEmail("ciao@gmail.com");
		newUser.setId(new Long(1));
		
		
	    Optional<UserEntity> opt = Optional.of(newUser);
		
		Mockito.when(repository.findById(newUser.getId())).thenReturn(opt);
		Mockito.when(repository.save(newUser)).thenReturn(newUser);
		
		UserEntity userTest = null;
		try {
			userTest = service.update(newUser);
			
		} catch (MulticanalitaDomainException e) {
			e.printStackTrace();
		}
		
		
		assertEquals(newUser, userTest);
		assertTrue(opt.isPresent());
		
		
	}
	@Test(expected = MulticanalitaDomainException.class)
	public void updateTestException() throws MulticanalitaDomainException {
		UserEntity newUser = new UserEntity();
		newUser.setNome("florian");
		newUser.setCognome("karici");
		newUser.setEmail("ciao@gmail.com");
		newUser.setId(new Long(1));
		
	    Optional<UserEntity> optNull = Optional.empty();
	  
		Mockito.when(repository.findById(newUser.getId())).thenReturn(optNull);
		//Mockito.when(repository.save(newUser)).thenReturn(newUser); non dovrebbe servire gia testato prima
		this.service.update(newUser);
		

		
	}
	
	@Test
	public void deleteTest() {
		
		UserEntity newUser = new UserEntity();
		newUser.setNome("utente");
		newUser.setCognome("pova");
		newUser.setEmail("prova@gmail.com");
		newUser.setId(new Long(1));
		

	    Optional<UserEntity> opt = Optional.of(newUser);
		
		Mockito.when(repository.findById(newUser.getId())).thenReturn(opt);
		Mockito.when(repository.save(newUser)).thenReturn(newUser);
		
		boolean deleteTest=false;
		try {
			deleteTest = service.deleteUserById(newUser.getId());
			
		} catch (MulticanalitaDomainException e) {
			e.printStackTrace();
		}
		
		
		//assertEquals(newUser, userTest);
		assertTrue(deleteTest);
		
		
	}
	
	
	@Test
	public void insertTest() {
		
		UserEntity newUser = new UserEntity();
		newUser.setNome("utente");
		newUser.setCognome("prova");
		newUser.setEmail("prova@gmail.com");
		newUser.setRuolo("dipendente");
		
		Mockito.when(repository.save(newUser)).thenReturn(newUser);
		
		UserEntity insertTest = new UserEntity();
		try {
			insertTest = service.insert(newUser);
		} catch (MulticanalitaDomainException e) {
			e.printStackTrace();
		}
		
		assertEquals(newUser, insertTest);
	}

}
