import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './routing/app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component';
import { EvolutiveComponent } from './components/evolutive/evolutive.component';
import { UsersService } from './services/users.service';
import { EvolutivaService } from './services/evolutive.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import {Moment} from 'moment/moment';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    EvolutiveComponent,
    HomeComponent
    
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,FormsModule,
    CommonModule,
    
   
    
  ],
  providers: [
    UsersService,
    EvolutivaService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
