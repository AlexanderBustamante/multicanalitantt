import { Evolutiva } from 'src/app/models/evolutiva.model';
import {EvolutivaService }  from '../../services/evolutive.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import * as moment from 'moment/moment';
import { User } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';


@Component({
  selector: 'app-evolutive',
  templateUrl: './evolutive.component.html',
  styleUrls: ['./evolutive.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class EvolutiveComponent implements OnInit {
  evolutive: Evolutiva[]= [];
  evolutivaID:Evolutiva = new Evolutiva;
  evolutivaTemp:Evolutiva = new Evolutiva();
  evolutivaNew: Evolutiva = new Evolutiva();
  modificaFlag:boolean = true;
  allUser: User[] = [];
  tempID :number =-1;
  showTuttoOk :boolean = false;
  inputAcronimo=""
  inputDescrizione= "";
  inputRichiedenteIsp ="";
  inputRefNTTD ="";
  evoMod = new Evolutiva();
  inputEvolutivaReleasePROD ="";
  data = new Date();
  @ViewChild('closebutton') closebutton: any;
  @ViewChild('modalEvoluitvaInfo') modalEvoInfo: any;
  @ViewChild('modaNewEvo') modalInsertEvo: any;
  @ViewChild('modalConfermaUpdate') modalConfermaUpdate: any;
  @ViewChild('modalRichiestaDelete') modalRichiestaDelete: any;
  @ViewChild('modalConfermaraDelete') modalConfermaraDelete: any;
  @ViewChild('modalConfermaInsert') modalConfermaInsert: any;
  @ViewChild('modalEvoluitvaModifica') modalEvoluitvaModifica:any;
  @ViewChild('modalErroreUpdate') modalErroreUpdate:any;
  @ViewChild('modalErroreInsert') modalErroreInsert:any;
  
  
 
  constructor(
  private evolutivaService : EvolutivaService,
  private router : Router,
  private modalService: NgbModal,
  private usersService: UsersService,

  ) { }

  ngOnInit(): void {
    this.modificaFlag=false;

    this.loadEvolutive();
    this.loadUser();
  }
  
  loadUser(){
    this.usersService.getUsers().subscribe((res: User[]) => {
      this.allUser = res;
      console.log("Risultato della get all user: ");
      console.log(this.allUser);
    }, err => {
      console.log(err);
    });

  }

  loadEvolutive(){
    this.evolutivaService.getEvolutive().subscribe((res:Evolutiva[]) => {
      this.evolutive = res;
      console.log(res);
    }, err => {
      console.log("errore nel caricamento delle evolutive"+ err);
    }
    );
      

  }
  confermaEliminazione(){
    this.modalService.open(this.modalRichiestaDelete,{
      windowClass: 'modaleNTTAngular',
      size:"sm",
      backdrop: 'static',
      centered: true
    })

  }


  closeAllModal(){
    this.modificaFlag=false;
    this.modalService.dismissAll()
    this.loadEvolutive();
    
  }
  riempiModaleConId(idRiga:number){
    this.tempID=idRiga;
    console.log("id evolutive" +this.tempID)
      this.evolutivaService.getEvolutiva(this.tempID).subscribe((res: Evolutiva) => {
        this.evolutivaID = res;
        console.log("riempi modalecon id:" +this.evolutivaID)
        this.data = this.evolutivaID.dtApproStima;
        
        console.log(moment(this.evolutivaID.dtApproStima).utc().format("YYYY-MM-DD, h:mm:ss a"));
      
        
        console.log(this.data);
        this.inputAcronimo=res.acronimo;
        this.inputDescrizione = res.descrizione;
 
    }, err => {
      console.log(err);
    });
  }

  openNTTmodal(content: any) {
    this.modalService.open(content,{
      windowClass: 'modaleNTTAngular',
      size:"xl",
      backdrop: 'static',
      centered: true,
      scrollable: true
      
    })
  }

  aprimodaleInfo(id:number){
    this.riempiModaleConId(id);
    this.openNTTmodal(this.modalEvoInfo)

  } 
  
  
  
  insertEvolutiva(){

  console.log("inserisco evolutiva"+ this.evolutivaNew)

   console.log("inserisco evolutiva"+ this.evolutivaNew.dtApproStima)
    this.evolutivaService.insert(this.evolutivaNew).subscribe(
      (res:Evolutiva) => {
        this.evolutivaNew = res;
        this.modalService.open(this.modalConfermaInsert,{
          windowClass: 'modaleNTTAngular',
          size:"sm",
          backdrop: 'static',
          centered: true
          
        })
        console.log(this.evolutivaNew);
        this.evolutivaNew = new Evolutiva();

        
        
      }, err => {
        this.modalService.open(this.modalErroreInsert,{
          windowClass: 'modaleNTTAngular',
          size:"sm",
          backdrop: 'static',
          centered: true
          
        })
        
        console.log("errore nel inserimento delle evolutive"+ err);
      }
      
    );
  }
  eliminaEvolutiva(obj_button:any){
    let id:number=obj_button.id;
    this.evolutivaService.deleteEvolutivaById(id).subscribe(
      (risultato) => {
        console.log(risultato);
        if(risultato == true){
          this.modalService.open(this.modalConfermaraDelete,{
            windowClass: 'modaleNTTAngular',
            size:"sm",
            backdrop: 'static',
            centered: true
          })
          
          
        }
      },  
      (errore) => {
        console.log(errore);
      }  
    );

  }

  modificaEvolutiva(){
    this.evoMod = this.evolutivaID;

    console.log(this.evoMod);
    this.evolutivaService.update(this.evoMod).subscribe(
      (res: Evolutiva) =>{
        this.modalService.open(this.modalConfermaUpdate,{
          windowClass: 'modaleNTTAngular',
          size:"sm",
          backdrop: 'static'})
       
        // this.router.navigateByUrl('/evolutiva');
        // this.loadEvolutive();s
       

      },
      (errore) => {
        this.modalService.open(this.modalErroreUpdate,{
          windowClass: 'modaleNTTAngular',
          size:"sm",
          backdrop: 'static'})
        
        console.log(errore);
      }
    );
  }
}

