import { User } from "./user.model";

export class Evolutiva{
    private  _id !:   number;
    private  _acronimo  !:        string;       
    private  _richiedenteISP !:string;
    private  _refNTTD !: string;
    private  _descrizione !:string;
    private  _evolutivaReleasePROD  !:boolean;
    private  _tipoRichiesta  !:      string;
    private  _loiNumOrdineNTTD !:string;
    private  _statoTesorettoEvo !:string;
    private  _rdaISP !:string;
    private  _tecSviluppo !:string;
	private _dtApproStima!: Date;
	private  _dtRichiesta !:Date;
	private  _dtInvioStima !:Date;
	private  _dtDocTecnica !:Date;
	private  _dtInoltroOfferta !:Date;
	private  _dtAvvioAttivita !:Date;
	private  _dtStartSvil !:Date;
	private  _dtSystem !:Date;
	private  _dtProd !:Date;
	private  _stato  !: string;         
	private  _stimaGGUISP  !:        number;
	private  _gGUtilizzateISP !:number;
	private  _residuoISP !:number;
	private  _stimaKISP !:number;
	private  _stimaKISPNoIVA !:number;
	private  _gGUutiliNNTD !:number;
	private  _oreNTTDUtilizzate !:number;
	private  _oreUtiliNTT !:number;
	private  _gGUtilizzatiNTTD !:number;
	private  _residuoGGISP !:number;
	private  _residuoGGNTTD !:number;
	private  _tariffaFerNTTD !:number;
	private  _refRealizzazNTTD !:User;
	private  _basketOreResiduo  !:   number;
	private  _basketGGResiduo !:number;
	private  _basketISPGGResidu !:number;
	private  _sottrarreATesoretto ! :number;
	private  _dispTesoResiduoDaAut !:number;
	private  _dispTesoResiduoAut ! :number;
	private  _gGTesoTot !:number;

    public toJSON() {
        return {
            id                   :this.id,
            acronimo             : this.acronimo,
            richiedenteISP     : this.richiedenteISP,
            refNTTD            : this.refNTTD,
            descrizione        : this.descrizione,
            evolutivaReleasePROD: this.evolutivaReleasePROD,
            tipoRichiesta           : this.tipoRichiesta ,
            loiNumOrdineNTTD   : this.loiNumOrdineNTTD ,
            statoTesorettoEvo   : this.statoTesorettoEvo,
            rdaISP              : this.rdaISP ,
            tecSviluppo         : this.tecSviluppo ,
            dtApproStima      : this.dtApproStima ,
            dtRichiesta        : this.dtRichiesta ,
            dtInvioStima        : this.dtInvioStima ,
            dtDocTecnica        : this.dtDocTecnica ,
            dtInoltroOfferta    : this.dtInoltroOfferta ,
            dtAvvioAttivita    : this.dtAvvioAttivita ,
            dtStartSvil         : this.dtStartSvil ,
            dtSystem            : this.dtSystem ,
            dtProd              : this.dtProd ,
            stato                       : this.stato  ,
            stimaGGUISP                : this.stimaGGUISP ,
            gGUtilizzateISP     : this.gGUtilizzateISP,
            residuoISP         : this.residuoISP ,
            stimaKISP           : this.stimaKISP ,
            stimaKISPNoIVA    : this.stimaKISPNoIVA,
            gGUutiliNNTD        : this.gGUutiliNNTD ,
            oreNTTDUtilizzate   : this.oreNTTDUtilizzate ,
            oreUtiliNTT         : this.oreUtiliNTT ,
            gGUtilizzatiNTTD    : this.gGUtilizzatiNTTD,
            residuoGGISP        : this.residuoGGISP ,
            residuoGGNTTD       : this.residuoGGNTTD ,
            tariffaFerNTTD      : this.tariffaFerNTTD ,
            refRealizzazNTTD    : this.refRealizzazNTTD ,
            basketOreResiduo   : this.basketOreResiduo ,
            basketGGResiduo    : this.basketGGResiduo ,
            basketISPGGResidu   : this.basketISPGGResidu ,
            sottrarreATesoretto : this.sottrarreATesoretto ,
            dispTesoResiduoDaAut: this.dispTesoResiduoDaAut ,
            dispTesoResiduoAut  : this.dispTesoResiduoAut ,
            gGTesoTot          : this.gGTesoTot
        };
      }

    public get id(): number {
        return this._id;
    }

    public set id(id: number) {
        this._id = id;
    }

    public get acronimo(): string {
        return this._acronimo;
    }

    public set acronimo(acronimo: string) {
        this._acronimo = acronimo;
    }

    public get richiedenteISP(): string
 {
        return this._richiedenteISP;
    }

    public set richiedenteISP(richiedenteISP: string
) {
        this._richiedenteISP = richiedenteISP;
    }

    public get refNTTD(): string
 {
        return this._refNTTD;
    }

    public set refNTTD(refNTTD: string
) {
        this._refNTTD = refNTTD;
    }

    public get descrizione(): string
 {
        return this._descrizione;
    }

    public set descrizione(Descrizione: string
) {
        this._descrizione = Descrizione;
    }

    public get evolutivaReleasePROD(): boolean {
        return this._evolutivaReleasePROD;
    }

    public set evolutivaReleasePROD(evolutivaReleasePROD: boolean) {
        this._evolutivaReleasePROD = evolutivaReleasePROD;
    }

    public get tipoRichiesta(): string {
        return this._tipoRichiesta;
    }

    public set tipoRichiesta(TipoRichiesta: string) {
        this._tipoRichiesta = TipoRichiesta;
    }

    public get loiNumOrdineNTTD(): string
 {
        return this._loiNumOrdineNTTD;
    }

    public set loiNumOrdineNTTD(LoiNumOrdineNTTD: string
) {
        this._loiNumOrdineNTTD = LoiNumOrdineNTTD;
    }

    public get statoTesorettoEvo(): string
 {
        return this._statoTesorettoEvo;
    }

    public set statoTesorettoEvo(StatoTesorettoEvo: string
) {
        this._statoTesorettoEvo = StatoTesorettoEvo;
    }

    public get rdaISP(): string
 {
        return this._rdaISP;
    }

    public set rdaISP(RdaISP: string
) {
        this._rdaISP = RdaISP;
    }

    public get tecSviluppo():string
 {
        return this._tecSviluppo;
    }

    public set tecSviluppo(TecSviluppo: string
) {
        this._tecSviluppo = TecSviluppo;
    }

    public get dtApproStima(): Date {
        return this._dtApproStima;
    }

    public set dtApproStima(DTApproStima: Date) {
        this._dtApproStima = DTApproStima;
    }

    public get dtRichiesta():Date
 {
        return this._dtRichiesta;
    }

    public set dtRichiesta(DTRichiesta:Date
) {
        this._dtRichiesta = DTRichiesta;
    }

    public get dtInvioStima(): Date
 {
        return this._dtInvioStima;
    }

    public set dtInvioStima(DTInvioStima: Date
) {
        this._dtInvioStima = DTInvioStima;
    }

    public get dtDocTecnica(): Date
 {
        return this._dtDocTecnica;
    }

    public set dtDocTecnica(DTDocTecnica: Date
) {
        this._dtDocTecnica = DTDocTecnica;
    }

    public get dtInoltroOfferta(): Date
 {
        return this._dtInoltroOfferta;
    }

    public set dtInoltroOfferta(DTInoltroOfferta: Date
) {
        this._dtInoltroOfferta = DTInoltroOfferta;
    }

    public get dtAvvioAttivita(): Date
 {
        return this._dtAvvioAttivita;
    }

    public set dtAvvioAttivita(DTAvvioAttivita:Date
) {
        this._dtAvvioAttivita = DTAvvioAttivita;
    }

    public get dtStartSvil(): Date
 {
        return this._dtStartSvil;
    }

    public set dtStartSvil(DTStartSvil: Date
) {
        this._dtStartSvil = DTStartSvil;
    }

    public get dtSystem(): Date
 {
        return this._dtSystem;
    }

    public set dtSystem(DTSystem: Date
) {
        this._dtSystem = DTSystem;
    }

    public get dtProd(): Date
 {
        return this._dtProd;
    }

    public set dtProd(DTProd: Date
) {
        this._dtProd = DTProd;
    }

    public get stato(): string {
        return this._stato;
    }

    public set stato(Stato: string) {
        this._stato = Stato;
    }

    public get stimaGGUISP(): number {
        return this._stimaGGUISP;
    }

    public set stimaGGUISP(StimaGGUISP: number) {
        this._stimaGGUISP = StimaGGUISP;
    }

    public get gGUtilizzateISP(): number
 {
        return this._gGUtilizzateISP;
    }

    public set gGUtilizzateISP(GGUtilizzateISP: number
) {
        this._gGUtilizzateISP = GGUtilizzateISP;
    }

    public get residuoISP(): number
 {
        return this._residuoISP;
    }

    public set residuoISP(ResiduoISP: number
) {
        this._residuoISP = ResiduoISP;
    }

    public get stimaKISP(): number
 {
        return this._stimaKISP;
    }

    public set stimaKISP(StimaKISP: number
) {
        this._stimaKISP = StimaKISP;
    }

    public get stimaKISPNoIVA():number
 {
        return this._stimaKISPNoIVA;
    }

    public set stimaKISPNoIVA(StimaKISPNoIVA: number
) {
        this._stimaKISPNoIVA = StimaKISPNoIVA;
    }

    public get gGUutiliNNTD():number
 {
        return this._gGUutiliNNTD;
    }

    public set gGUutiliNNTD(GGUutiliNNTD: number
) {
        this._gGUutiliNNTD = GGUutiliNNTD;
    }

    public get oreNTTDUtilizzate(): number
 {
        return this._oreNTTDUtilizzate;
    }

    public set oreNTTDUtilizzate(OreNTTDUtilizzate: number
) {
        this._oreNTTDUtilizzate = OreNTTDUtilizzate;
    }

    public get oreUtiliNTT(): number
 {
        return this._oreUtiliNTT;
    }

    public set oreUtiliNTT(OreUtiliNTT: number
) {
        this._oreUtiliNTT = OreUtiliNTT;
    }

    public get gGUtilizzatiNTTD(): number
 {
        return this._gGUtilizzatiNTTD;
    }

    public set gGUtilizzatiNTTD(GGUtilizzatiNTTD:number
) {
        this._gGUtilizzatiNTTD = GGUtilizzatiNTTD;
    }

    public get residuoGGISP(): number
 {
        return this._residuoGGISP;
    }

    public set residuoGGISP(ResiduoGGISP: number
) {
        this._residuoGGISP = ResiduoGGISP;
    }

    public get residuoGGNTTD():number
 {
        return this._residuoGGNTTD;
    }

    public set residuoGGNTTD(ResiduoGGNTTD: number
) {
        this._residuoGGNTTD = ResiduoGGNTTD;
    }

    public get tariffaFerNTTD(): number
 {
        return this._tariffaFerNTTD;
    }

    public set tariffaFerNTTD(TariffaFerNTTD: number
) {
        this._tariffaFerNTTD = TariffaFerNTTD;
    }

    public get refRealizzazNTTD(): User
 {
        return this._refRealizzazNTTD;
    }

    public set refRealizzazNTTD(RefRealizzazNTTD: User
) {
        this._refRealizzazNTTD = RefRealizzazNTTD;
    }

    public get basketOreResiduo(): number {
        return this._basketOreResiduo;
    }

    public set basketOreResiduo(BasketOreResiduo: number) {
        this._basketOreResiduo = BasketOreResiduo;
    }

    public get basketGGResiduo(): number
 {
        return this._basketGGResiduo;
    }

    public set basketGGResiduo(BasketGGResiduo: number
) {
        this._basketGGResiduo = BasketGGResiduo;
    }

    public get basketISPGGResidu(): number
 {
        return this._basketISPGGResidu;
    }

    public set basketISPGGResidu(BasketISPGGResidu: number
) {
        this._basketISPGGResidu = BasketISPGGResidu;
    }

    public get sottrarreATesoretto(): number
 {
        return this._sottrarreATesoretto;
    }

    public set sottrarreATesoretto(SottrarreATesoretto: number
) {
        this._sottrarreATesoretto = SottrarreATesoretto;
    }

    public get dispTesoResiduoDaAut(): number
 {
        return this._dispTesoResiduoDaAut;
    }

    public set dispTesoResiduoDaAut(DispTesoResiduoDaAut: number
) {
        this._dispTesoResiduoDaAut = DispTesoResiduoDaAut;
    }

    public get dispTesoResiduoAut(): number
 {
        return this._dispTesoResiduoAut;
    }

    public set dispTesoResiduoAut(DispTesoResiduoAut: number
) {
        this._dispTesoResiduoAut = DispTesoResiduoAut;
    }

    public get gGTesoTot(): number {
        return this._gGTesoTot;
    }

    public set gGTesoTot(GGTesoTot: number) {
        this._gGTesoTot = GGTesoTot;
    }

}
