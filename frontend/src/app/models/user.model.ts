export class User {
    private _id!: number;
    private _nome!: string;
    private _cognome!: string;
    private _email!: string;
    private _ruolo!: string;
    private _posizione!: string;
    private _idCluApp!: number;

    public get id(): number {
        return this._id;
    }

    public set id(id: number) {
        this._id = id;
    }

    public get nome(): string {
        return this._nome;
    }

    public set nome(nome: string) {
        this._nome = nome;
    }

    public get cognome(): string {
        return this._cognome;
    }

    public set cognome(cognome: string) {
        this._cognome = cognome;
    }

    public get email(): string {
        return this._email;
    }

    public set email(email: string) {
        this._email = email;
    }

    public get ruolo(): string {
        return this._ruolo;
    }

    public set ruolo(ruolo: string) {
        this._ruolo = ruolo;
    }

    public get posizione(): string {
        return this._posizione;
    }
    public set posizione(posizione: string) {
        this._posizione = posizione;
    }
    public get idCluApp(): number {
        return this._idCluApp;
    }

    public set idCluApp(idCluApp: number) {
        this._idCluApp = idCluApp;
    }

    public toJSON() {
        return {
            id:this.id,
          nome: this.nome,
          cognome: this.cognome,
          email : this.email, 
          ruolo : this.ruolo,
          posizione : this.posizione,
          idCluApp : this.idCluApp
        };
      }
  }
  
  