import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from '.././components/user/user.component';
import { EvolutiveComponent } from '../components/evolutive/evolutive.component';
import { HomeComponent } from '../components/home/home.component';
const routes: Routes = [
  //{path: "", redirectTo: "", pathMatch: "full"},
  {path:'utente', component: UserComponent},
  {path:'evolutive', component: EvolutiveComponent},
  {path:'home', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
