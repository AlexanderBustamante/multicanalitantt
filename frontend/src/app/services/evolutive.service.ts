import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Evolutiva } from '../models/evolutiva.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EvolutivaService {

  constructor(private httpClient: HttpClient) { 
  }

  EVOLUTIVA_URL = `${environment.basicURL}` + '/evolutiva';
  
  getEvolutive(): Observable<Evolutiva[]> {
    return this.httpClient.get<Evolutiva[]>(this.EVOLUTIVA_URL); 
  }
  getEvolutiva(id: number): Observable<Evolutiva> {
    const url = `${this.EVOLUTIVA_URL}/${id}`;
    return this.httpClient.get<Evolutiva>(url);
  } 
  update(evoTemp:Evolutiva): Observable<Evolutiva>{
    const url = `${this.EVOLUTIVA_URL}/update`;
    return this.httpClient.put<Evolutiva>(url,evoTemp);
  }
  deleteEvolutivaById(id:number): Observable<boolean>{
    const url=`${this.EVOLUTIVA_URL}/${id}`;
    return this.httpClient.delete<boolean>(url);
  }
  insert(evo :Evolutiva): Observable<Evolutiva>{
    console.log(evo);
    const url=`${this.EVOLUTIVA_URL}/insert`;
    return this.httpClient.post<Evolutiva>(url,evo);
  }


}

