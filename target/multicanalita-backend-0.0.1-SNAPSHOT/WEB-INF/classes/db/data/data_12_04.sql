DROP TABLE IF EXISTS Dipendenti;

CREATE TABLE Dipendenti (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cognome VARCHAR(250) NOT NULL,
  email VARCHAR(250) DEFAULT NULL,
  pwd 	VARCHAR(50) NOT NULL,
  ruolo varchar(50) 
  posizione varchar(50)
);
INSERT INTO Dipendenti (nome, cognome, email) VALUES
  ('Andrea', 'Caria', 'AndreaMarco.Caria@nttdata.com'),
  ('Marisa', 'Mastroleo', 'Marisa.Mastroleo@nttdata.com'),
  ('Florian', 'Karici', 'Florian.Karici@nttdata.com'),
  ('Andrea', 'Luca', 'Andrea.Luca@nttdata.com')
  ;